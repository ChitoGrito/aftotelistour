<?php

namespace app\modules\main\controllers;

use app\modules\main\models\MainCaruselImages;
use app\modules\main\models\Posts;
use yii\web\Controller;
use Yii;

/**
 * Default controller for the `main` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
 
    public function actionIndex()
    {
        $carusel = MainCaruselImages::find()->all();
        $posts_preview = Posts::find()
        ->where(['<=', 'upload_at', strtotime("now")])
        ->andWhere(['show_it' => true])
        ->orderBy(['upload_at'=> SORT_DESC])->limit(6)->all();
        return $this->render('index', [
            'carusel' => $carusel,
            'posts_preview' => $posts_preview,
        ]);
    }
}
