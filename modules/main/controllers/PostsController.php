<?php

namespace app\modules\main\controllers;

use app\modules\admin\models\PostsSearch;
use app\modules\main\models\Posts;
use app\modules\user\models\PostsLikes;
use app\modules\user\models\PostsViews;
use yii\web\NotFoundHttpException;
use yii\web\Controller;
use Yii;

class PostsController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new PostsSearch([
            'front' => true,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'posts' => $dataProvider->getModels(),
            'searchModel' => $searchModel,
        ]);
    }

    public function actionView($id)
    {
        $post = $this->findModel($id);
        if (!Yii::$app->user->isGuest) {
            $posts_views = new PostsViews();
            $posts_views->makeViews(Yii::$app->user->identity->id, $id);
        }
        return $this->render('view', [
            'post' => $post,
        ]);
    }

    public function actionVote($id)
    {
        if (!Yii::$app->user->isGuest) {
            $post_likes = new PostsLikes();
            $likes = $post_likes->makeLike(Yii::$app->user->identity->id, $id);

            return $this->renderAjax('vote', [
                'id' => $id,
                'like_' . $id => $likes,
            ]);
        } else {
            return $this->redirect(['view', 'id' => $id]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
