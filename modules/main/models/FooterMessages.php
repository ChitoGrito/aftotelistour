<?php

namespace app\modules\main\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%footer_messages}}".
 *
 * @property int $id
 * @property int $created_at
 * @property string|null $name
 * @property string $email
 * @property string|null $text
 */
class FooterMessages extends \yii\db\ActiveRecord
{

    /* public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    } */
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%footer_messages}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'text', 'user_ip'], 'required'],
            [['created_at'], 'integer'],
            [['text'], 'string'],
            [['name'], 'string', 'max' => 30],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Время создания',
            'user_ip' => 'IP пользователя',
            'name' => 'Имя',
            'email' => 'E-mail',
            'text' => 'Текст сообщения',
            'viewed' => 'Просмотренно',
        ];
    }

    public function makeViewed()
    {
        $this->viewed = true;
        $this->save();
    }

    public function makeNotViewed()
    {
        $this->viewed = false;
        $this->save();
    }    
}
