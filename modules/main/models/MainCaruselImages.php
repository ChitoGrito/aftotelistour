<?php

namespace app\modules\main\models;

use Yii;

/**
 * This is the model class for table "{{%main_carusel_images}}".
 *
 * @property int $id
 * @property string|null $img_name
 * @property string|null $img_text_big
 * @property string|null $img_text_small
 * @property int|null $show_it
 */
class MainCaruselImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%main_carusel_images}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['show_it'], 'boolean'],
            [['img_src', 'img_text_big', 'img_text_small'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'img_src' => Yii::t('app', 'Img Name'),
            'img_text_big' => 'Верхний тект (большой)',
            'img_text_small' => 'Нижний текст (маленький)',
            'show_it' => 'Отображение',
        ];
    }

    public function saveIt($file_names)
    {
        foreach ($file_names as $name) {
            $image = $this->findOne(['img_src' => $name]);
            if (!isset($image->img_src)) {
                $this->img_src = $name;
                $this->save();
                unset($this->img_src);
            }
        }
    }

    public function resizeImage($src)
    {
        // файл
        $filename = $src;

        // задание максимальной ширины и высоты
        $width = 1600;
        $height = 463;

        // тип содержимого
        header('Content-Type: image/jpeg');

        // получение новых размеров
        list($width_orig, $height_orig) = getimagesize($filename);

        $ratio_orig = $width_orig / $height_orig;

        if ($width / $height > $ratio_orig) {
            $width = $height * $ratio_orig;
        } else {
            $height = $width / $ratio_orig;
        }

        // ресэмплирование
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        // вывод
        imagejpeg($image_p, null, 100);
    }
}
