<?php

use yii\widgets\Pjax;
use yii\helpers\Html;

?>

<?php
$css_class_likes = 'icons-grey';
if (${'like_' . $id}['isLiked']) {
    $css_class_likes = 'text-primary';
}

Pjax::begin([
    'enablePushState' => false,
    'id' => 'tab' . $id . '_pjax',
    'options' => ['data-pjax-container' => 'tab_' . $id]
]);
?>
<table style="border-collapse: collapse">
    <tr>
        <td style="vertical-align: middle; height: 50px; width:50px"><?= Html::a('<i class="icon-heart icon2x '.$css_class_likes.' float-left"></i>', ['vote', 'id' => $id]) ?></td>
        <td style="vertical-align: middle; height: 50px">
            <p style="margin-bottom: 0"><?= ${'like_' . $id}['likes_count'] ?></p>
        </td>
    </tr>
</table>
<?php Pjax::end(); ?>