<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

$this->title = Html::encode('Блог');

$this->params['breadcrumbs'][] = $this->title;

?>

<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">Блог</h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <div class="margin-bottom">
                <div class="line">
                    <div class="line">
                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]);
                        $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
                        $params = [
                            'prompt' => 'Выберите страну...',
                            'class' => 'form-control',
                        ];
                        ?>
                        <div class="col-xs-4">
                            <?php
                            echo '<label class="control-label">Дата публикации</label>';
                            echo DatePicker::widget([
                                'model' => $searchModel,
                                'attribute' => 'date_from_create',
                                'attribute2' => 'date_to_create',
                                'type' => DatePicker::TYPE_RANGE,
                                'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                                'pluginOptions' => [
                                    'format' => 'dd.mm.yyyy',
                                    'autoclose' => true,
                                ],
                                'options' => [
                                    // you can hide the input by setting the following
                                    'autocomplete' => 'off',
                                ]
                            ]);
                            ?>
                        </div>


                        <div class="col-xs-4">
                            <?= $form->field($searchModel, 'country_id', ['inputOptions' => ['autocomplete' => 'off']])->dropDownList($countries, $params)->label('Выберите страну') ?>
                        </div>

                        <div class="col-xs-4">
                            <?= $form->field($searchModel, 'author_name', [
                                'inputOptions' => [
                                    'autocomplete' => 'off'
                                ]
                            ])->label('Автор') ?>
                        </div>
                    </div>

                    <div class="form-group col-md-2 col-md-offset-5">
                        <div class="float-left">
                            <?= Html::submitButton('Поиск', ['class' => 'button border-radius background-primary text-white']) ?>
                        </div>
                        <div class="margin-left-100">
                            <?= Html::a('Сброс', ['index'], [
                                'class' => 'button border-radius button-primary-stroke'
                            ]) ?>
                        </div>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <div class="line">
                <div class="margin text-center">
                    <?php foreach ($posts as $post) : ?>
                        <?php
                        //debug($post);
                        ?>
                        <div class="s-12 m-12 l-4 margin-bottom">
                            <div class="padding-2x block-bordered border-radius">

                                <?= Html::a(Html::img('@' . Yii::$app->params['countries.icons'] . $post->country->img_src, [
                                    'alt' => $post->country->name,
                                    'width' => '100px',
                                    'height' => '100px',
                                    'class' => 'coutry-ico text-primary margin-bottom-30',
                                ]), ['view', 'id' => $post->id], ['class' => 'image-hover-zoom']) ?>

                                <h2 class="text-thin"><?= $post->main_theme ?></h2>
                                <p class="margin-bottom-30"><?= $post->sub_theme ?></p>

                                <div class="row margin-top">
                                    <table style="border-collapse: collapse">
                                        <tr>
                                            <td style="vertical-align: middle;"><i class="icon-user icon text-primary float-left"></i></td>
                                            <td style="vertical-align: middle; padding-left: 0;">
                                                <p style="margin-bottom: 0"><?= $post->author->username ?></p>
                                            </td>

                                            <td style="vertical-align: middle;"><i class="icon-clock icon text-primary float-left"></i></td>
                                            <td style="vertical-align: middle; padding-left: 0;">
                                                <p style="margin-bottom: 0"><?= date('d.m.Y H:i', $post->upload_at) ?></p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <?= Html::a('ЧИТАТЬ ДАЛЕЕ', ['view', 'id' => $post->id], ['class' => 'button border-radius background-primary text-size-12 text-white text-strong margin-top']) ?>

                                <?php if (!Yii::$app->user->isGuest) : ?>
                                    <div class="line margin-top-30 vert-centered">
                                        <div class="col-xs-6" style="height: 45px;">
                                            <?php
                                            //debug(Yii::$app->user->identity->id);
                                            $css_class_likes = 'icons-grey';
                                            foreach ($post->likes as $like) {
                                                if ($like->author_id == Yii::$app->user->identity->id) {
                                                    $css_class_likes = 'text-primary';
                                                }
                                            }
                                            Pjax::begin([
                                                'enablePushState' => false,
                                                'id' => 'tab' . $post->id . '_pjax',
                                                'options' => [
                                                    'data-pjax-container' => 'tab_' . $post->id,
                                                ],
                                            ]);
                                            ?>
                                            <table style="border-collapse: collapse">
                                                <tr>
                                                    <td style="vertical-align: middle; height: 50px; width:50px"><?= Html::a('<i class="icon-heart icon2x ' . $css_class_likes . ' float-left"></i>', ['vote', 'id' => $post->id]) ?></td>
                                                    <td style="vertical-align: middle; height: 50px">
                                                        <p style="margin-bottom: 0"><?= count($post->likes) ?></p>
                                                    </td>
                                                </tr>
                                            </table>
                                            <?php Pjax::end(); ?>
                                        </div>
                                        <div class="col-xs-6" style="height: 45px;">
                                            <?php
                                            $css_class_views = 'icons-grey';
                                            foreach ($post->views as $view) {
                                                if ($view->author_id == Yii::$app->user->identity->id) {
                                                    $css_class_views = 'text-primary';
                                                }
                                            }
                                            ?>
                                            <i class="icon-eye icon2x <?= $css_class_views ?> float-left margin-left"></i>
                                            <p class="margin-top-10" style="margin-bottom: 0;"><?= count($post->views) ?></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

        </div>
    </article>
</main>