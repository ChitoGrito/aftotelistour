<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;

$this->title = Html::encode($post->country->name . ': ' . $post->main_theme);
$this->params['breadcrumbs'][] = ['label' => 'Блог', 'url' => ['/main/posts/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- MAIN -->
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= $post->country->name ?></h1>
        </header>

        <div class="section background-white">
            <div class="line">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <div class="line">
                <h2 class="text-size-30"><?= $post->main_theme ?></h2>
                <?= $post->body ?>
            </div>
            <div class="line margin-top">
                <table style="border-collapse: collapse">
                    <tr>
                        <td style="vertical-align: middle; height: 50px; width:50px"><i class="icon-user icon text-primary float-left"></i></td>
                        <td style="vertical-align: middle; padding-left: 0;">
                            <p style="margin-bottom: 0"><?= $post->author->username ?></p>
                        </td>

                        <td style="vertical-align: middle; height: 50px; width:50px"><i class="icon-clock icon text-primary float-left"></i></td>
                        <td style="vertical-align: middle; padding-left: 0;">
                            <p style="margin-bottom: 0"><?= date('d.m.Y H:i', $post->upload_at) ?></p>
                        </td>


                        <?php if (!Yii::$app->user->isGuest) : ?>
                            <td>
                                <?php
                                //debug(Yii::$app->user->identity->id);
                                $css_class_likes = 'icons-grey';
                                foreach ($post->likes as $like) {
                                    if ($like->author_id == Yii::$app->user->identity->id) {
                                        $css_class_likes = 'text-primary';
                                    }
                                }

                                Pjax::begin([
                                    'enablePushState' => false,
                                    'id' => 'tab' . $post->id . '_pjax',
                                    'options' => [
                                        'data-pjax-container' => 'tab_' . $post->id,
                                    ],
                                ]);
                                ?>

                                <table style="border-collapse: collapse">
                                    <tr>
                                        <td style="vertical-align: middle; height: 50px; width:50px"><?= Html::a('<i class="icon-heart icon2x ' . $css_class_likes . ' float-left"></i>', ['vote', 'id' => $post->id]) ?></td>
                                        <td style="vertical-align: middle; height: 50px">
                                            <p style="margin-bottom: 0"><?= count($post->likes) ?></p>
                                        </td>
                                    </tr>
                                </table>
                                <?php Pjax::end(); ?>
                            </td>
                        <?php endif; ?>

                        <td style="vertical-align: middle; height: 50px; width:50px"><i class="icon-eye icon2x text-primary float-left"></i></td>
                        <td style="vertical-align: middle; height: 50px">
                            <p style="margin-bottom: 0"><?= count($post->views) ?></p>
                        </td>

                    </tr>
                </table>

            </div>
            <div class="line">
                <?php echo \yii2mod\comments\widgets\Comment::widget([
                    'model' => $post,
                    'maxLevel' => 4,
                    'dataProviderConfig' => [
                        'pagination' => [
                            'pageSize' => 10
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </article>
</main>