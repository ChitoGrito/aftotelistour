<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Breadcrumbs;
use app\modules\main\components\widgets\Alert;

$this->title = Yii::t('app', 'TITLE_CONTACT');
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>

            <div class="line">
                <div class="margin">
                    <h1 class="text-thin margin-bottom-30 margin-top"><?= Yii::t('app', 'BUISINES_INQUIRIES_QUESTIONS') ?></h1>
                    <h2 class="text-thin margin-bottom-30 margin-top"><?= Yii::t('app', 'THANK_YOU') ?></h2>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?php
                    $form = ActiveForm::begin([
                        'id' => 'contact-form',
                        'options' => [
                            'class' => 'customform',
                        ],
                    ]);
                    echo $form->errorSummary($model);
                    ?>

                    <div class="line">
                        <h2 class="text-thin margin-bottom-30 margin-top">Представтесь:</h2>
                        <?= $form->field($model, 'name', [
                            'inputOptions' => ['autocomplete' => 'off'],
                            'options' => [
                                'tag' => false,
                                'class' => 's-12 m-12 l-6',
                            ],
                        ])->textInput([
                            'class' => 'required name border-radius',
                            'placeholder' => 'Ваше имя',
                            'title' => 'Ваше имя',
                        ])->label(false) ?>
                    </div>

                    <div class="line">
                        <h2 class="text-thin margin-bottom-30 margin-top">Введите адрес вашей электронной почты:</h2>
                        <?= $form->field($model, 'email', [
                            'inputOptions' => ['autocomplete' => 'off'],
                            'options' => [
                                'tag' => false,
                                'class' => 's-12 m-12 l-6',
                            ],
                        ])->textInput([
                            'class' => 'required email border-radius',
                            'placeholder' => 'Ваше имя',
                            'title' => 'Ваше имя',
                        ])->label(false) ?>
                    </div>

                    <div class="line">
                        <h2 class="text-thin margin-bottom-30 margin-top">Назовите тему вашего сообщения:</h2>
                        <?= $form->field($model, 'subject', [
                            'inputOptions' => ['autocomplete' => 'off'],
                            'options' => [
                                'tag' => false,
                                'class' => 's-12 m-12 l-6',
                            ],
                        ])->textInput([
                            'class' => 'required theme border-radius',
                            'placeholder' => 'Тема сообщения',
                            'title' => 'Тема сообщения',
                        ])->label(false) ?>
                    </div>

                    <div class="line">
                        <h2 class="text-thin margin-bottom-30 margin-top">Введите текст вашего сообщения:</h2>
                        <?= $form->field($model, 'body', [
                            'inputOptions' => ['autocomplete' => 'off'],
                            'options' => [
                                'tag' => false,
                                'class' => 's-12 m-12 l-6',
                            ],
                        ])->textarea([
                            'rows' => 6,
                            'class' => 'required message border-radius',
                            'placeholder' => 'Текст сообщения',
                            'title' => 'Текст сообщения',
                        ])->label(false) ?>
                    </div>

                    <div class="line">
                        <h2 class="text-thin margin-bottom-30 margin-top">Докажите, что вы не робот!</h2>
                        <?= $form->field($model, 'verifyCode', [
                            'inputOptions' => ['autocomplete' => 'off'],
                            'options' => [
                                'tag' => false,
                                'class' => 'required subject border-radius',
                            ],
                        ])->widget(Captcha::className(), [
                            'captchaAction' => '/main/contact/captcha',
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ])->label(false) ?>
                    </div>

                    <div class="line margin-top">
                        <?= Html::submitButton(Yii::t('app', 'BUTTON_SUBMIT'), ['class' => 'submit-form button background-primary border-radius text-white', 'name' => 'contact-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>