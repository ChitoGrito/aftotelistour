<?php

use app\modules\main\components\helpers\NamesHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\main\components\widgets\Alert;

$this->title = Yii::$app->name;

?>
<main role="main">
    <!-- Main Carousel -->
    <section class="section background-dark">
        <div class="line">
            <div class="margin">
                <?= Alert::widget([
                    'options' => [
                        'class' => 'footer-alert text-thin text-white',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="line">
            <div class="carousel-fade-transition owl-carousel carousel-main carousel-nav-white carousel-wide-arrows">
                <?php foreach ($carusel as $image) : ?>
                    <?php if ($image->show_it) : ?>
                        <div class="item">
                            <div class="s-12 center">
                                <img src=<?= Url::to('@' . Yii::$app->params['main.carusel.photos'] . $image->img_src) ?> alt="">
                                <div class="carousel-content">
                                    <div class="padding-2x">
                                        <div class="s-12 m-12 l-8">
                                            <p class="text-white text-s-size-20 text-m-size-40 text-l-size-60 margin-bottom-40 text-thin text-line-height-1"><?= $image->img_text_big ?></p>
                                            <p class="text-white text-size-16 margin-bottom-40"><?= $image->img_text_small ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <!-- From posts -->
    <section class="section background-white">
        <div class="line">
            <h2 class="text-thin headline text-center text-s-size-30 margin-bottom-50">Последние <span class="text-primary">Статьи</span></h2>
            <div class="carousel-default owl-carousel carousel-wide-arrows">
                <?php if (!empty($posts_preview)) : ?>
                    <?php for ($i = 0; $i < count($posts_preview); $i++) : ?>
                        <?php
                        $post_image = NamesHelper::findImageNames($posts_preview[$i]->body);
                        $post_image = $post_image[0];
                        ?>
                        <div class="item">
                            <div class="margin">
                                <?php if ($posts_preview[$i]->show_it) : ?>
                                    <div class="s-12 m-12 l-6">
                                        <div class="image-border-radius margin-m-bottom">
                                            <div class="margin">
                                                <div class="s-12 m-12 l-4 margin-m-bottom">
                                                    <?= Html::a(Html::img('@web' . Yii::$app->params['posts.photos'] . $post_image['img_src'], [
                                                        'alt' => $post_image['alt'],
                                                    ]), ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'image-hover-zoom']) ?>
                                                </div>
                                                <div class="s-12 m-12 l-8 margin-m-bottom">
                                                    <h3><?= Html::a($posts_preview[$i]->main_theme, ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'text-dark text-primary-hover']) ?></h3>
                                                    <p><?= $posts_preview[$i]->sub_theme ?></p>
                                                    <?= Html::a('Читать далее...', ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'text-more-info text-primary-hover']) ?>
                                                    <!--  <a class="text-more-info text-primary-hover" href="/">Читать далее...</a> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php
                                $i++;
                                if (isset($posts_preview[$i])) {
                                    $post_image = NamesHelper::findImageNames($posts_preview[$i]->body);
                                    $post_image = $post_image[0];
                                }
                                ?>
                                <?php if (isset($posts_preview[$i])) : ?>
                                    <?php if ($posts_preview[$i]->show_it) : ?>
                                        <div class="s-12 m-12 l-6">
                                            <div class="image-border-radius">
                                                <div class="margin">
                                                    <div class="s-12 m-12 l-4 margin-m-bottom">
                                                        <?= Html::a(Html::img('@web' . Yii::$app->params['posts.photos'] . $post_image['img_src'], [
                                                            'alt' => $post_image['alt'],
                                                        ]), ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'image-hover-zoom']) ?>
                                                    </div>
                                                    <div class="s-12 m-12 l-8">
                                                        <h3><?= Html::a($posts_preview[$i]->main_theme, ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'text-dark text-primary-hover']) ?></h3>
                                                        <p><?= $posts_preview[$i]->sub_theme ?></p>
                                                        <?= Html::a('Читать далее...', ['posts/view', 'id' => $posts_preview[$i]->id], ['class' => 'text-more-info text-primary-hover']) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <hr class="break margin-top-bottom-0">

    <!-- Section 2 -->
    <section class="section background-primary text-center">
        <div class="line">
            <div class="s-12 m-10 l-8 center">
                <h2 class="headline text-thin text-s-size-30">Сам Себе Гид</h2>
                <p class="text-size-20">Это блог для людей, которые хотят и любят путешествовать, смотреть на страну не из окна туристического автобуса, проезжающего мимо самого интересного!</p>
            </div>
        </div>
    </section>

    <!-- Section 3 -->
    <section class="section background-white">
        <div class="full-width text-center">
            <img class="center margin-bottom-30" style="margin-top: -250px;" src="img/obo_mne/puteshestvije.png" alt="">
            <div class="line">
                <h2 class="headline text-thin text-s-size-30">Как <span class="text-primary">Присоединиться</span> К группе?</h2>
                <p class="text-size-20 text-s-size-16 text-thin">Если у Вас появился интерес присоединиться к нам, то Вы можете задать вопросы в формах обратной связи. Как это в принципе происходит? Люди сообщают даты в которые они могут поехать в путешествие. Собирается костяк группы не менее 4 человек. Согласуем удобные для всех даты вылета и возвращения.На эти даты каждый сам покупает билет и сообщает номер бронирования. На нужное количество людей в зависимости от пожеланий ( кому какой номер нужен — одиночный или двуспальный) бронируем гостиницу. В тот интервал, когда группа уже находится в месте дислокации, возможно прилететь позже и улететь раньше. Встретим и отвезем в аэропорт по необходимости.</p>
            </div>
        </div>
    </section>

</main>