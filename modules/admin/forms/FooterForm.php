<?php

namespace app\modules\admin\forms;

use app\modules\admin\models\FooterCol1;
use yii\base\Model;
use Yii;
 
/**
 * Footer form
 *
 * public $fields = array(
 *       0 => array(
 *           'id' => 1,
 *           'theme' => null,
 *           'text' => null,
 *           'img_link' => null,
 *           'img_src' => null,
 *           'read_more_link' => null,
 *           'show_it' => null,
 *       ),
 *       1 => array(
 *           'id' => 2,
 *           'theme' => null,
 *           'text' => null,
 *           'img_link' => null,
 *           'img_src' => null,
 *           'read_more_link' => null,
 *           'show_it' => null,
 *       ),
 *
 */
class FooterForm extends Model
{
    public $id = 1;
    public $theme = null;
    public $text = null;
    public $img_link = null;
    public $img_src;
    public $read_more_link = null;
    public $show_it = null;

    public $id2 = 2;
    public $theme2 = null;
    public $text2 = null;
    public $img_link2 = null;
    public $img_src2;
    public $read_more_link2 = null;
    public $show_it2 = null;


    public function __construct()                               //при обьявлении класса собираю значения переменных из базы, приравниваю соотвествующие поля из БД к переменным класса
    {
        $model = FooterCol1::find()->asArray()->all();
        if (!empty($model)) {
            foreach ($model as $num => $row) {
                foreach ($row as $k => $v) {
                    if ($num < 1) {                             //в БД должно быть только 2 записи
                        $this->{$k} = $v;                       //данные для первого блока подвала
                    } else {
                        $this->{$k.'2'} = $v;                   //данные для второго блока подвала (с двойкой на конце)
                    }
                }
            }
        }
    }

    public function rules()
    {
        return [
            ['theme', 'filter', 'filter' => 'trim'],
            ['theme2', 'filter', 'filter' => 'trim'],
            [['theme', 'text', 'show_it'], 'required'],
            [['theme2', 'text2', 'show_it2'], 'required'],
        ];
    }

    public function saveIt()                                              //функция сохранения данных из полей этой формы в БД
    {
        if ($this->validate()) {
            $model_array = [];                                            //запихиваю названия и значения пеерменных в массив. пример массива выше.
            $vars_array = get_object_vars($this);
            foreach ($vars_array as $key => $value) {
                if (stristr($key, '2') === false) {                       // переменные, где нет 2 в названии в 0 элемент массива
                    $model_array[0][$key] = $value;
                } else {                                                  // где есть - во второй
                    $key = stristr($key, '2', true);                      // плюс подчищаю двойку (остаются только те символы строки, что были до двойки)
                    $model_array[1][$key] = $value;
                }
            }

            foreach ($model_array as $array) {                            //используем ранее сформированный массив для навигации по переменным и полям БД
                $colOneModel = FooterCol1::findOne($array['id']);
                if (empty($colOneModel)) {                                // ищем соответсвующую строку
                    $colOneModel = new FooterCol1();
                }
                foreach ($array as $fName => $fValue) {
                    $colOneModel->{$fName} = $fValue;                     // пробегаемся по всем значениям в массиве, приравниваем их к соотв. свойству обьекта из БД
                }
                if (!$colOneModel->save()) {                              // сохраняем
                    return false;
                }
                unset($colOneModel);
            }
        }
        return true;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'theme' => 'Тема',
            'text' => 'Текст',
            'img_link' => 'Ссылка с фотографии',
            'img_src' => 'Путь к фотографии',
            'read_more_link' => 'Ссылка для "Подробнее"',
            'show_it' => 'Отображение',
            'id2' => 'ID',
            'theme2' => 'Тема',
            'text2' => 'Текст',
            'img_link2' => 'Ссылка с фотографии',
            'img_src2' => 'Путь к фотографии',
            'read_more_link2' => 'Ссылка для "Подробнее"',
            'show_it2' => 'Отображение',
        ];
    }
}
