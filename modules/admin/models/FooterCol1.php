<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "{{%footerCol_1}}".
 *
 * @property int $id
 * @property string $theme
 * @property string $text
 * @property string|null $img_link
 * @property string|null $img_src
 * @property string|null $read_more_link
 * @property int|null $show_it
 */
class FooterCol1 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%footerCol_1}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['theme', 'text'], 'required'],
            [['text'], 'string'],
            [['show_it'], 'boolean'],
            [['theme'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'theme' => 'Тема',
            'text' => 'Текст',
            'img_link' => 'Ссылка с фотки',
            'img_src' => 'Место хранения фотки',
            'read_more_link' => 'Ссылка для "Подробнее"',
            'show_it' => 'Отображения записи',
        ];
    }
}
