<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\Navigation;
use app\modules\admin\models\NavigationSubp2;

/**
 * This is the model class for table "{{%navigationSubp}}".
 *
 * @property int $id
 * @property string $nav_id
 * @property string $name
 * @property int $url
 */
class NavigationSubp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%navigationSubp}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'url'], 'required'],
            //[['url'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['nav_id'], 'exist', 'skipOnError' => true, 'targetClass' => Navigation::className(), 'targetAttribute' => ['nav_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nav_id' => Yii::t('app', 'Nav ID'),
            'name' => Yii::t('app', 'NAVIGATION_Name'),
            'url' => Yii::t('app', 'NAVIGATION_Url'),
        ];
    }

    public function getNavigation()
    {
        return $this->hasOne(Navigation::className(), ['id' => 'nav_id']);
    }

    public function getNavigationSubp2()
    {
        return $this->hasMany(NavigationSubp2::className(), ['nav_nav_id' => 'id']);
    }
}
