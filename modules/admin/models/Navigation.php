<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\NavigationSubp;

/**
 * This is the model class for table "{{%navigation}}".
 *
 * @property int $id
 * @property string $url
 * @property string $name
 * @property int $access
 * @property int $subparagraph
 *
 * Модель для получения данных из БД
 * для вывода пунктов меню (основных)
 *
 */
class Navigation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%navigation}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'name', 'access'], 'required'],
            [['access', 'subparagraph'], 'integer'],
            [['url', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => Yii::t('app', 'NAVIGATION_Url'),
            'name' => Yii::t('app', 'NAVIGATION_Name'),
            'access' => Yii::t('app', 'NAVIGATION_Access'),
            'subparagraph' => Yii::t('app', 'NAVIGATION_subparagraph'),
        ];
    }

    public function getNavigationSubp()
    {
        return $this->hasMany(NavigationSubp::className(), ['nav_id' => 'id']);
    }
}
