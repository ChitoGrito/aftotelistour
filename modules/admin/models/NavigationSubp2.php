<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\NavigationSubp;

/**
 * This is the model class for table "{{%navigationSubp2}}".
 *
 * @property int $id
 * @property int $nav_nav_id
 * @property string $name
 * @property int $url
 * @property int $access
 */
class NavigationSubp2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%navigationSubp2}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nav_nav_id', 'name', 'url', 'access'], 'required'],
            [['nav_nav_id', 'url', 'access'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nav_nav_id' => Yii::t('app', 'Nav Nav ID'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'access' => Yii::t('app', 'Access'),
        ];
    }

    public function getNavigationSubp()
    {
        return $this->hasOne(NavigationSubp::className(), ['id' => 'nav_nav_id']);
    }
}
