<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\main\models\Posts;

/**
 * This is the model class for table "{{%countries}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property Posts[] $posts
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%countries}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название страны',
            'img_src' => 'Иконка',
        ];
    }

    /**
     * Gets query for [[Posts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Posts::className(), ['country_id' => 'id']);
    }
}
