<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "{{%footerCol_2}}".
 *
 * @property int $id
 * @property string $title
 * @property string|null $ico
 * @property string|null $sub_title
 * @property string|null $content
 * @property int|null $show_it
 */
class FooterCol2 extends \yii\db\ActiveRecord
{
    
    public $p_types = array();

    public function __construct()
    {
        parent::__construct();
        $this->nameTypes();
    }

    public static function tableName()
    {
        return '{{%footerCol_2}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sub_title', 'content'], 'required'],
            [['show_it'], 'integer'],
            [['title', 'ico', 'sub_title', 'content'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'ico' => Yii::t('app', 'Ico'),
            'sub_title' => 'Тип данных',
            'content' => 'Содержание',
            'show_it' => 'Отображение',
        ];
    }

    public function setIcon()
    {
        if (file_exists(__DIR__.'/../../../config/footer-icons.php')) {
            $types = require __DIR__.'/../../../config/footer-icons.php';
            if (isset($types[$this->sub_title]['ico'])) {
                $this->ico = $types[$this->sub_title]['ico'];
                return true;
            } else {
                $this->ico = null;
                return false;
            }
        } else {
            return false;
        }
    }

    public function nameTypes()
    {
        if (file_exists(__DIR__.'/../../../config/footer-icons.php')) {
            $types = require_once __DIR__.'/../../../config/footer-icons.php';
            if (is_array($types)) {
                foreach ($types as $name => $type) {
                    $this->p_types[$name] = $type['rus'];
                }
            }
        } else {
            throw new \yii\web\HttpException(500, 'Файл настройки подвала отсутствуеи, или не читабелен');
        }
    }
}
