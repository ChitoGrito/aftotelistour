<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\widgets\Pjax;

$this->title = 'Управление фотографиями';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка карусели основной страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="add-text">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php

    $form = ActiveForm::begin();

    echo $form->field($db_image, 'img_text_big', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($db_image, 'img_text_small', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($db_image, 'show_it', ['inputOptions' => ['autocomplete' => 'off']])->checkbox();

    echo Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']);
    ActiveForm::end();
  
    ?>
</div>
