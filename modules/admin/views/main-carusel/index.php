<?php

use yii\helpers\Html;
use yii\helpers\Url;
use Yii;
use kartik\file\FileInput;

$this->title = 'Настройка карусели основной страницы';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="main-carusel">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Управление фотографиями', ['add-photos'], ['class' => 'btn btn-success col-sm']) ?>
    </p>
    <div class="row">
        <?php foreach ($images as $image) : ?>
            <div class="col-md-4">
                <div class="thumbnail">
                <style type="text/css">
                    a:hover {
                        text-decoration: none; /* Убирает подчеркивание для ссылок */
                        color: blue;
                    }
                    </style>
                    <a href=<?= Url::toRoute(['add-text', 'id' => $image->id]) ?>>
                        <img src=<?= Url::to('@'.Yii::$app->params['main.carusel.photos'].$image->img_src) ?> alt="Lights" style="width:100%">
                        <div class="caption">
                            <p><?= $image->img_text_big ?></p>
                            <p><?= $image->img_text_small ?></p>
                            <?php
                            if ($image->show_it) {
                                echo '<div class="alert alert-success" role="alert">'.
                                'Фото отображается!'.
                                '</div>';
                            } else {
                                echo '<div class="alert alert-danger" role="alert">'.
                                'Фото не отображается!'.
                                '</div>';
                            }
                            ?>
                        </div>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>