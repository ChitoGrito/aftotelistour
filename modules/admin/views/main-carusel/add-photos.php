<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use yii\widgets\Pjax;

$this->title = 'Управление фотографиями';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка карусели основной страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="add-photos">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?php

    $form = ActiveForm::begin();
    echo $form->field($images_files, 'images[]')->widget(FileInput::classname(), [
        //'disabled' => true,
        'options' => ['multiple' => true, 'accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview' => $preview['for_preview'],
            'initialPreviewConfig' => $preview['initialPreviewConfig'],
            'initialPreviewAsData'=>true,
            'overwriteInitial'=>false,
            'maxFileSize'=>8800,
            'previewFileType' => 'any',
            'showCaption' => false,
            /* 'showRemove' => false, */
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Выбрать фото',
        ]
    ]);
    echo Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']);
    ActiveForm::end();
  
    ?>
</div>
