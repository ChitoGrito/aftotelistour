<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\NavigationSubpSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подпункты 1 уровня для пункта "'.$prev_nav_point->name.'"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ADMIN_NAVIGATION'), 'url' => ['/admin/navigation/index']];
$this->params['breadcrumbs'][] = ['label' => 'Пункт: '.$prev_nav_point->name, 'url' => ['/admin/navigation/view', 'id' => $prev_nav_point->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="navigation-subp-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Navigation Subp'), ['create', 'main_id' => $prev_nav_point->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'nav_id',
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                }
            ],
            'url',
            //'access',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
