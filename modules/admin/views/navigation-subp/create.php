<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NavigationSubp */

$this->title = 'Создать новый подпункт для пункта: '.$prev_nav_point->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ADMIN_NAVIGATION'), 'url' => ['/admin/navigation/index']];
$this->params['breadcrumbs'][] = ['label' => 'Пункт: '.$prev_nav_point->name, 'url' => ['/admin/navigation/view', 'id' => $prev_nav_point->id]];
$this->params['breadcrumbs'][] = ['label' => 'Подпункты 1 уровня для пункта "'.$prev_nav_point->name.'"', 'url' => ['/admin/navigation-subp/index', 'main_id' => $prev_nav_point->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="navigation-subp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
