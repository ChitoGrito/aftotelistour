<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\NavigationSubp */

$this->title = 'Подпункт: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ADMIN_NAVIGATION'), 'url' => ['/admin/navigation/index']];
$this->params['breadcrumbs'][] = ['label' => 'Пункт: '.$prev_nav_point->name, 'url' => ['/admin/navigation/view', 'id' => $prev_nav_point->id]];
$this->params['breadcrumbs'][] = ['label' => 'Подпункты 1 уровня для пункта "'.$prev_nav_point->name.'"', 'url' => ['/admin/navigation-subp/index', 'main_id' => $prev_nav_point->id]];
$this->params['breadcrumbs'][] = $this->title;

\yii\web\YiiAsset::register($this);
?>
<div class="navigation-subp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nav_id',
            'name',
            'url:url',
            'access',
        ],
    ]) ?>

</div>
