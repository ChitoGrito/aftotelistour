<?php

use app\modules\admin\models\Navigation;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Navigation */
/* @var $form yii\widgets\ActiveForm */
$model->subparagraph = 0;
$model->access = 0;
?>

<div class="navigation-form">

    <?php
    $form = ActiveForm::begin();
    $items = [
        '0' => 'Для всех',
        '1' => 'Для не залогиненный',
        '2' => 'Для залогиненных',
    ];
    $params = [
        'prompt' => 'Выберите статус...'
    ];

    $count = Navigation::find()->count();

    for ($i = 1; $i <= $count; $i++) {
        $items_2["$i"] = "$i";
    }
    ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'access')->dropDownList($items, $params); ?>
    
    <?= $form->field($model, 'subparagraph')->dropDownList($items_2, $params); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
