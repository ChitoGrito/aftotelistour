<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Navigation */

$this->title = 'Пункт: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ADMIN_NAVIGATION'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="navigation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Подпункты', [Url::to(['/admin/navigation-subp/index']), 'main_id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'url',
            'name',
            //'access',
            [
                'attribute' => 'access',
                'format' => 'raw',
                'value' => function ($data) {
                    $ret = null;
                    switch ($data->access) {
                        case 0:
                            return '<span class="text-success">Для всех</span>';
                            break;
                        case 1:
                            return '<span class="text-success">Для гостей</span>';
                            break;
                        case 2:
                            return '<span class="text-success">Для залогиненных</span>';
                            break;
                    }
                    return '<span class="text-success">Ошибка, не выводися</span>';
                }
            ],
            'subparagraph',
        ],
    ]) ?>

</div>
