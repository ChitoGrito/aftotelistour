<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\FooterMessagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сообщения из подвала';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка "подвала"', 'url' => ['/admin/footer/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-messages-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $key, $index, $grid) {
            //$class = $index % 2 ? 'odd' : 'even';
            $class = null;
            if (!$model->viewed == 1) {
                $class = 'viewed';
            }
            return [
                //'key' => $key,
                //'index' => $index,
                'class' => $class
            ];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'created_at',
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from',
                    'attribute2' => 'date_to',
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => false,
                    ],
                    'options' => [
                        // you can hide the input by setting the following
                        'autocomplete' => 'off',
                    ]
                ]),
                'attribute' => 'created_at',
                'contentOptions' => [ 'style' => 'width: 25%;'],
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
            
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->name), ['view', 'id' => $model->id]);
                }
            ],

            'email',
            //'text:ntext',

            [
                'attribute' => 'user_ip',
                'value' => function ($data) {
                    return long2ip($data->user_ip);
                }
            ],
            //'viewed',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {delete}',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
            ],
        ],
    ]); ?>


</div>
