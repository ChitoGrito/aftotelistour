<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;

$this->title = 'Сообщение от: '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка "подвала"', 'url' => ['/admin/footer-messages/index']];
$this->params['breadcrumbs'][] = ['label' => 'Сообщения из "подвала"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="footer-messages-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],
            'name',
            'email',
            'text:ntext',
            //'user_ip',
            [
                'attribute' => 'user_ip',
                'value' => function ($data) {
                    return long2ip($data->user_ip);
                }
            ],
        ],
    ]) ?>


<?php
$form = ActiveForm::begin([
    //'layout' => 'horizontal',
    'options' => ['class' => 'form-inline'],
]);
?>


<?= $form->field($model, 'viewed', ['inputOptions' => ['autocomplete' => 'off']])->checkbox()->label('Пометить, как не прочитанное') ?>         
<?= Html::submitButton('Сохранить и выйти', ['class' => 'btn btn-primary mb-2']) ?>

    
<?php ActiveForm::end();?>


</div>
