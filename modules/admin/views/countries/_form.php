<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Countries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="countries-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
    <?= $form->field($files_upload, 'images')->widget(FileInput::classname(), [
        'options' => [/* 'multiple' => true,  */'accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview' => $preview['for_preview'],
            'initialPreviewConfig' => $preview['initialPreviewConfig'],
            'initialPreviewAsData'=>true,
            'overwriteInitial'=>false,
            'maxFileSize'=>8800,
            'previewFileType' => 'any',
            'showCaption' => false,
            /* 'showRemove' => false, */
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Выбрать фото',
        ]
    ])?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
