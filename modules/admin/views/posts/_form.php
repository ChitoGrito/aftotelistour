<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use app\modules\user\constants\Consts;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use kartik\datetime\DateTimePicker;

?>

<div class="margin">
    <?php
    //Pjax::begin();
    $form = ActiveForm::begin([
        'id' => 'new-post-form',
        'options' => [
            //'class' => 'form-horizontal',
            'data-pjax' => true,
        ],
    ]);
    $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
    $params = [
        'prompt' => 'Выберите страну...',
        'class' => 'form-control',
    ];
    echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'country_id', ['inputOptions' => ['autocomplete' => 'off']])->dropDownList($countries, $params)->label('Выберите страну') ?>

    <?php
    if (!empty($model->upload_at)) {
        $model->upload_at = date('d.m.Y H:i:s', $model->upload_at);
    }
    echo '<label class="control-label">Время публикации</label>';
    echo DateTimePicker::widget([
        'type' => DateTimePicker::TYPE_BUTTON,
        'layout' => '{picker} {remove} {input}',
        'options' => [
            'type' => 'text',
            'readonly' => true,
            'class' => 'text-muted small',
            'style' => 'border:none;background:none',
        ],
        'model' => $model,
        'attribute' => 'upload_at',
        'name' => 'upload_at',
        'value' => null,
        'readonly' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'dd.mm.yyyy hh:ii',
            'todayHighlight' => true,
            'startDate' => 'd',
        ]
    ]);
    echo Html::error($model, 'upload_at', ['class' => 'daterror']);
    ?>

    <?= $form->field($model, 'main_theme', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

    <?= $form->field($model, 'sub_theme', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['rows' => 3, 'class' => 'form-control']) ?>



    <?php
    echo $form->field($model, 'body')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['posts/image-upload', 'id' => $model->id]),
            'imageManagerJson' => Url::to(['posts/images-get', 'id' => $model->id]),
            'fileUpload' => Url::to(['posts/file-upload', 'id' => $model->id]),
            'fileManagerJson' => Url::to(['posts/files-get', 'id' => $model->id]),
            'imageDelete' => Url::to(['posts/image-delete', 'id' => $model->id]),
            'fileDelete' => Url::to(['posts/file-delete', 'id' => $model->id]),
            'plugins' => [
                'fullscreen',
            ],
        ],
        'plugins' => [
            'filemanager' => 'vova07\imperavi\bundles\FileManagerAsset',
            'imagemanager' => 'vova07\imperavi\bundles\ImageManagerAsset',

        ],
    ]);
    //echo Html::error($model, 'body', ['class' => 'daterror']);
    echo $form->field($model, 'show_it', ['inputOptions' => ['autocomplete' => 'off']])->checkbox();
    ?>

    <div class="form-group">
        <?php
        $nextSteps = $model->nextSteps();
        //debug($model->fromArchive);
        foreach ($nextSteps as $step) {
            if ($step['name'] == Consts::FLOW_ARCHIVE) {
                $step['meta']['actionLabel'] = 'Сохранить и положить в архив';
            }
            echo Html::submitButton(Html::encode($step['meta']['actionLabel']), ['class' => 'btn btn-success']);
        }
        ?>
    </div>

    <?php
    ActiveForm::end();
    ?>
</div>