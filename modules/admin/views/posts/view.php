<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\user\constants\Consts;
use app\assets\AppAsset;
use Codeception\Command\Shared\Style;

$this->title = $model->main_theme;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Статьи', 'url' => ['/admin/posts/index']];
$this->params['breadcrumbs'][] = $this->title;

AppAsset::register($this);
?>

<div class="posts-view margin-bottom-40">
    <?= Html::errorSummary($model, [
        'class' => 'error-summary',
    ]) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'country.name',

            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            [
                'attribute' => 'updated_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            [
                'attribute' => 'upload_at',
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            'author.username',
            'main_theme',
            'sub_theme',
            'body:ntext',

            [
                'attribute' => 'show_it',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->show_it ? '<span class="label label-success">ДА</span>' : '<span class="label label-danger">НЕТ</span>';
                }
            ],

            /* [
                'attribute' => 'enable_likes',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->show_it ? '<span class="text-success">Вкл</span>' : '<span class="text-danger">Выкл</span>';
                }
            ],

            [
                'attribute' => 'enable_dislikes',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->show_it ? '<span class="text-success">Вкл</span>' : '<span class="text-danger">Выкл</span>';
                }
            ],

            [
                'attribute' => 'enable_coments',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->show_it ? '<span class="text-success">Вкл</span>' : '<span class="text-danger">Выкл</span>';
                }
            ], */

            [
                'attribute' => Consts::STATUS_ATTR,
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span class="label ' . $data->getWorkflowStatus()->getMetadata('labelClass') . '">' . $data->getWorkflowStatus()->getLabel() . '</span>';
                },
            ],

        ],
    ]) ?>

</div>

<!-- MAIN -->
<main role="main">
    <div class="line" style="padding: 0;">
        <?php
        $nextSteps = $model->nextSteps();
        //debug($nextSteps);
        echo '<div class="margin-bottom">';
        foreach ($nextSteps as $step) {
            echo Html::a(Html::encode($step['meta']['actionLabel']), ['get-action', 'id' => $model->id, 'to_status' => $step['name']], ['class' => 'button button-primary-stroke margin-right']);
        }
        echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'button cancel-btn',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить эту статью?',
                'method' => 'post',
            ],
        ]);
        echo '</div>';
        ?>
    </div>
    <!-- Content -->
    <article>
        <div class="line">
            <div class="margin text-center">
                <?php foreach ($posts as $post) : ?>
                    <?php
                    //debug($post);
                    ?>
                    <div class="s-12 m-12 l-4 margin-bottom">
                        <div class="padding-2x block-bordered border-radius">

                            <?= Html::a(Html::img('@' . Yii::$app->params['countries.icons'] . $post->country->img_src, [
                                'alt' => $post->country->name,
                                'width' => '100px',
                                'height' => '100px',
                                'class' => 'coutry-ico text-primary margin-bottom-30',
                            ]), ['view', 'id' => $model->id], ['class' => 'image-hover-zoom']) ?>

                            <h2 class="text-thin"><?= $post->main_theme ?></h2>
                            <p class="margin-bottom-30"><?= $post->sub_theme ?></p>

                            <div class="row margin-top">
                                <table style="border-collapse: collapse">
                                    <tr>
                                        <td style="vertical-align: middle;"><i class="icon-user icon text-primary float-left"></i></td>
                                        <td style="vertical-align: middle; padding-left: 0;">
                                            <p style="margin-bottom: 0"><?= $post->author->username ?></p>
                                        </td>

                                        <td style="vertical-align: middle;"><i class="icon-clock icon text-primary float-left"></i></td>
                                        <td style="vertical-align: middle; padding-left: 0;">
                                            <p style="margin-bottom: 0"><?= date('d.m.Y H:i') ?></p>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <?= Html::a('ЧИТАТЬ ДАЛЕЕ', ['view', 'id' => $model->id], ['class' => 'button border-radius background-primary text-size-12 text-white text-strong margin-top']) ?>

                            <?php if (!Yii::$app->user->isGuest) : ?>
                                <div class="line margin-top-30 vert-centered">
                                    <div class="col-xs-6" style="height: 45px;">
                                        <table style="border-collapse: collapse">
                                            <tr>
                                                <td style="vertical-align: middle; height: 50px; width:50px"><?= Html::a('<i class="icon-heart icon2x icons-grey float-left"></i>', ['view', 'id' => $model->id]) ?></td>
                                                <td style="vertical-align: middle; height: 50px">
                                                    <p style="margin-bottom: 0"><?= 5 ?></p>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-xs-6" style="height: 45px;">
                                        <i class="icon-eye icon2x icons-grey float-left margin-left"></i>
                                        <p class="margin-top-10" style="margin-bottom: 0;"><?= 5 ?></p>
                                    </div>
                                </div>
                            <?php endif; ?>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($model->country->name) ?></h1>
        </header>

        <div class="section background-white">
            <div class="line">
                <h2 class="text-size-30"><?= $model->main_theme ?></h2>
                <?= $model->body ?>
            </div>
        </div>
    </article>
</main>