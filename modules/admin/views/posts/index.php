<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use kartik\date\DatePicker;
use app\modules\main\components\widgets\Alert;
use app\modules\user\constants\Consts;

$this->title = Html::encode('Статьи');
$this->params['breadcrumbs'][] = ['label' => Html::encode(Yii::t('app', 'NAV_ADMIN')), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = Html::encode($this->title);
?>
<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Html::encode('Создать статью'), ['process'], ['class' => 'btn btn-success margin-right-40']) ?>
        <?= Html::a(Html::encode('Архив'), ['archive'], ['class' => 'btn btn-default']) ?>
        <?= Html::a(Html::encode('Управление странами'), ['/admin/countries/index'], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'country_id',
                'value' => 'country.name',
                'filter' => ArrayHelper::map(Countries::find()->all(), 'id', 'name'),
                'options' => ['width' => '150'],
            ],

            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from_upload',
                    'attribute2' => 'date_to_upload',
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => false,
                    ],
                    'options' => [
                        // you can hide the input by setting the following
                        'autocomplete' => 'off',
                    ]
                ]),
                'attribute' => 'upload_at',
                'contentOptions' => ['style' => 'width: 20%;'],
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_from_create',
                    'attribute2' => 'date_to_create',
                    'type' => DatePicker::TYPE_RANGE,
                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                        'autoclose' => false,
                    ],
                    'options' => [
                        // you can hide the input by setting the following
                        'autocomplete' => 'off',
                    ]
                ]),
                'attribute' => 'updated_at',
                'contentOptions' => ['style' => 'width: 20%;'],
                'format' => ['date', 'php:d.m.Y H:i:s'],
            ],

            [
                'attribute' => 'show_it',
                'format' => 'raw',
                'value' => function ($data) {
                    return $data->show_it ? '<span class="label label-success">ДА</span>' : '<span class="label label-danger">НЕТ</span>';
                }
            ],

            [
                'attribute' => Consts::STATUS_ATTR,
                'format' => 'raw',
                'value' => function ($data) {
                    return '<span class="label ' . $data->getWorkflowStatus()->getMetadata('labelClass') . '">' . $data->getWorkflowStatus()->getLabel() . '</span>';
                },
                'filter' => ArrayHelper::map(
                    $searchModel->allSteps(
                        $unset = array(
                            Consts::FLOW_NEW,
                            Consts::FLOW_ARCHIVE
                        )
                    ),
                    'name',
                    'label'
                ),
            ],

            [
                'attribute' => 'main_theme',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->main_theme), ['view', 'id' => $model->id]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
                'template' => '{view} {delete}',
            ],
        ],
    ]); ?>


</div>