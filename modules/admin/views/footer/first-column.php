<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

$this->title = 'Изменить первую колонку подвала';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка "подвала"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-first-column">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $form = ActiveForm::begin();
    echo '<hr>';
    echo '<h3>Верхняя часть:</h3>';
    echo $form->field($model, 'theme', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($model, 'text', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($model, 'show_it', ['inputOptions' => ['autocomplete' => 'off']])->checkbox();
    
    echo '<hr>';

    echo '<h3>Нижняя часть:</h3>';
    echo $form->field($model, 'theme2', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($model, 'text2', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($files_upload, 'images')->widget(FileInput::classname(), [
        'options' => [/* 'multiple' => true,  */'accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview' => $preview['for_preview'],
            'initialPreviewConfig' => $preview['initialPreviewConfig'],
            'initialPreviewAsData'=>true,
            'overwriteInitial'=>false,
            'maxFileSize'=>8800,
            'previewFileType' => 'any',
            'showCaption' => false,
            /* 'showRemove' => false, */
            'showUpload' => false,
            'browseClass' => 'btn btn-primary btn-block',
            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
            'browseLabel' =>  'Выбрать фото',
        ]
    ]);
    echo $form->field($model, 'img_link2', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($model, 'read_more_link2', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']);
    echo $form->field($model, 'show_it2', ['inputOptions' => ['autocomplete' => 'off']])->checkbox();
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
