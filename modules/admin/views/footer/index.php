<?php
 
use yii\helpers\Html;
 
/* @var $this yii\web\View */
/* @var $model \app\modules\user\models\User */
 
$this->title = 'Настройка "подвала"';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-footer-index">
    <h1><?= Html::encode($this->title) ?></h1>
 
    <p>
        <?= Html::a('Настройка первой колонки', ['footer/first-column'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Настройка второй колонки', ['footer/second-column'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Просмотр сообщений из формы', ['footer-messages/index'], ['class' => 'btn btn-primary']) ?>
    </p>
</div>
