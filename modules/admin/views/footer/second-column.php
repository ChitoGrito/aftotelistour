<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$this->title = 'Пункты второй колонки подвала';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка "подвала"', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="footer-second-column">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        echo Html::a('Добавить строку контактов', ['second-column-create'], ['class' => 'btn btn-success']);
        ?>
    </p>

    <?php Pjax::begin(); ?>
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            [
                'attribute' => 'sub_title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Html::encode($model->sub_title), ['second-column-update', 'id' => $model->id]);
                },
                'filter' => $searchModel->p_types,
            ],

            'content',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
                'template' => '{update} {delete}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-wrench"></span>',
                            ['second-column-update', 'id' => $model->id]
                        );
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-remove-circle"></span>', ['second-column-delete', 'id' => $model->id], [
                            'data' => [
                                'confirm' => 'Вы уверены, что хотите удалить этот пункт контактов?',
                                'method' => 'post',
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>