<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Создание нового пункта второй колонки подвала';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'NAV_ADMIN'), 'url' => ['/admin/default/index']];
$this->params['breadcrumbs'][] = ['label' => 'Настройка "подвала"', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Пункты второй колонки подвала', 'url' => ['second-column']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="footer-first-column">

<h1><?= Html::encode($this->title) ?></h1>

<?php
$form = ActiveForm::begin();

$params = [
    'prompt' => 'Выберите тип данных...',
    'class' => 'form-control',
];

echo $form->field($model, 'sub_title', ['inputOptions' => ['autocomplete' => 'off']])->dropDownList($model->p_types, $params);
echo $form->field($model, 'content', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']);
echo $form->field($model, 'show_it', ['inputOptions' => ['autocomplete' => 'off']])->checkbox();
?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
</div>