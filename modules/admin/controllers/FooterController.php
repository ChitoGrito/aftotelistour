<?php

namespace app\modules\admin\controllers;

use app\modules\admin\forms\FooterForm;
use app\modules\admin\models\FilesUpload;
use app\modules\admin\models\FooterCol1;
use app\modules\admin\models\FooterCol2;
use app\modules\admin\models\FooterCol2Search;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\web\Controller;
use Yii;

class FooterController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFirstColumn()
    {
        $model = new FooterForm();                                                                              //данные из бд подтягиваются в конструкторе класса, если такие есть
        $file_type = 'images';                                                                                  // определяем тип заружаемых данных
        $path = '/var/www/start2/docs/'.Yii::$app->params['footer.img.path'];
        $file_names = false;

        if (empty($model->img_src2)) {
            $files_upload = new FilesUpload(['scenario' => FilesUpload::SCENARIO_NEW]);
            $files_upload->new_one = true;                                                                      // обозначаем, что это новая запись
        } else {
            $files_upload = new FilesUpload([
                'scenario' => FilesUpload::SCENARIO_UPDATE,
            ]);
        }

        $preview = $files_upload->initialPreviewGenerator($model, Yii::$app->params['footer.img.path']);                                              // получаю данные для превью в виджете уже загруженных фоток

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $files_upload->images = UploadedFile::getInstances($files_upload, 'images');                        //загружаем фотку
            if (!empty($files_upload->images)) {                                                                //если файлы загружены - сохраняем их, запись в бд идет в модели
                $this->clearDir($path);                                                                         // на всякий подчищаем всю папку, нужна только одна текущая фотка
                $file_names = $files_upload->uploadThis($file_type, $path);                                     //сохраняем
                if (!$file_names) {
                    Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены из-за фотографии!');
                    return $this->redirect(['first-column']);
                }
            }
            if ($file_names) {
                foreach ($file_names as $name) {
                    $model->img_src2 = $name;
                    $model->img_src = $name;                                                                    //пишу в img_src первого тоже, для корректной работы функции initialPreviewGenerator
                }
            }
            if ($model->saveIt()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                return $this->redirect(['first-column']);
            }
        }

        return $this->render('first-column', [
            'model' => $model,
            'files_upload' => $files_upload,
            'preview' => $preview,
        ]);
    }

    public function actionSecondColumn()
    {
        $searchModel = new FooterCol2Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //$dataTypes = $searchModel->

        return $this->render('second-column', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSecondColumnCreate()
    {
        $model = new FooterCol2();
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->setIcon()) {
                Yii::$app->getSession()->setFlash('danger', 'Ошибка назначения икнонки');
            }

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['second-column']);
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                return $this->redirect(['second-column-create']);
            }
        }

        return $this->render('second-column-form', [
            'model' => $model,
        ]);
    }

    public function actionSecondColumnUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->setIcon()) {
                Yii::$app->getSession()->setFlash('danger', 'Ошибка назначения икнонки');
            }

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['second-column']);
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                return $this->redirect(['second-column-create']);
            }
        }

        return $this->render('second-column-form', [
            'model' => $model,
        ]);
    }

    public function actionSecondColumnDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', 'Данные удалены!');
        return $this->redirect(['second-column']);
    }

    public function actionDeleteFiles($img_id, $img_name)                         //экшн для удаления фоток из картик инпута
    {
        $path = __DIR__.'/../../../'.Yii::$app->params['footer.img.path'];
        $this->clearDir($path);
        $models = FooterCol1::find()->where(['img_src' => $img_name])->all();                                     //подчищаем названия файлов в базе, в обоих строках
        foreach ($models as $model) {
            $model->img_src = null;
            $model->save();
        }
        return '{}';                                                               //надо вернуть это, чтобы картик инпут не вываливался в ошибку
    }

    protected function clearDir($path)                          //подчистка всей папки, так как нам нужна только одна фотка, а на сревере их кол-во может разростаться
    {
        $photos = scandir($path, 1);
        //debug($photos);
        array_pop($photos);
        array_pop($photos);
        if (!empty($photos)) {
            foreach ($photos as $photo) {
                unlink($path.$photo);
            }
        }
    }

    protected function findModel($id)
    {
        if (($model = FooterCol2::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
