<?php

namespace app\modules\admin\controllers;

use app\modules\main\models\Posts;
use app\modules\user\constants\Consts;
use app\modules\admin\models\PostsSearch;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use app\modules\main\components\helpers\NamesHelper;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class PostsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /* Подробные коменты смотри в контроллере MyPosts в модуле юзера, очень похоже впадлу перепечатывать */

    public function actions()           // так то путь к сохранению фоток прописан в самом модуле, тут прописаны чтобы не вызывать ошибки
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
                'translit' => true,
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']], // These options are by default.
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetFilesAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.txt', '*.md', '*.pdf', '*.doc', '*.docx', '*.zip', '*.rar', '*.7z']], // These options are by default.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false, // For any kind of files uploading.
                //'unique' => false,
                'translit' => true,
            ],
            'image-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()                               // отображаются все статьи, что есть в БД, кроме статуса "Архив"
    {
        $searchModel = new PostsSearch([
            'front' => false,
            'isArchive' => false,
            'locationFront' => false,
        ]);
        //debug($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionArchive()
    {
        $searchModel = new PostsSearch([
            'front' => false,
            'isArchive' => true,
            'locationFront' => false,
        ]);
        //debug($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('archive', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionProcess($id = false)
    {
        if ($id) {                                                           // понимаем, это новая новость, или редактирование
            $model = $this->findModel($id);
            $model->locationFront = false;
        } else {
            $model = new Posts(['locationFront' => true]);                   // задается место создания обьекта, в данном случае - фронтэнд, тобишь юзер
            $model->enterWorkflow();                                         // входим в воркфлоу, начальный статус
            $model->show_it = true;
            $model->saveIt();
            return $this->redirect(['process', 'id' => $model->id]);
        }

        if ($model->getWorkflowStatus()->getId() !== Consts::FLOW_ADMIN_PROCESS) {
            if ($model->checkAccess(Consts::FLOW_ADMIN_PROCESS)) {
                $model->sendToStatus(Consts::FLOW_ADMIN_PROCESS);
                $model->update();
            } else {
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkAccess(Consts::FLOW_ADMIN_CHECKING)) {
                $model->post_status = Consts::FLOW_ADMIN_CHECKING;
            } else {
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
            }
            if ($model->validate()) {
                if ($model->update()) {
                    Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('process', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = Posts::findOne($id);
        $model->locationFront = false;

        $status = $model->getWorkflowStatus()->getId();

        switch ($status) {
            case Consts::FLOW_ADMIN_PROCESS:
                if (!$model->checkAccess($status)) {
                    Yii::$app->getSession()->setFlash('danger', 'Заполните все поля, и сохранитесь, иначе предпросмотр не возможен!');
                    return $this->redirect(['process', 'id' => $id]);
                }
                break;

            case Consts::FLOW_ARCHIVE:
                break;

            case Consts::FLOW_USER_DENIED:
                break;

            default:
                if ($model->checkAccess(Consts::FLOW_ADMIN_CHECKING)) {
                    $model->sendToStatus(Consts::FLOW_ADMIN_CHECKING);
                    $model->update();
                }
                break;
        }

        if (empty($model->country_id) or empty($model->main_theme) or empty($model->sub_theme) or empty($model->body)) {
            Yii::$app->getSession()->setFlash('danger', 'Заполните все поля, иначе предпросмотр не возможен!');
            return $this->redirect(['process', 'id' => $id]);
        }

        $searchModel = new PostsSearch([
            'front' => true,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'posts' => $dataProvider->getModels(),
        ]);
    }

    public function actionGetAction($id, $to_status)
    {
        $model = $this->findModel($id);

        $model->locationFront = false;

        if (!$model->checkAccess($to_status)) {
            throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
        }

        switch ($to_status) {
            case Consts::FLOW_ADMIN_PROCESS:
                return $this->redirect(['process', 'id' => $id]);
                break;

            case Consts::FLOW_ARCHIVE:
                $model->sendToStatus(Consts::FLOW_ARCHIVE);
                $model->update();
                return $this->redirect(['archive']);
                break;

            case Consts::FLOW_ADMIN_PUBLISHED:
                $model->{Consts::STATUS_ATTR} = Consts::FLOW_ADMIN_PUBLISHED;
                if ($model->validate()) {
                    $model->update();
                    return $this->redirect(['index']);
                }
                return $this->render('view', [
                    'model' => $model,
                ]);
                break;

            case Consts::FLOW_ADMIN_CHECKING:
                $model->sendToStatus(Consts::FLOW_ADMIN_CHECKING);
                $model->update();
                return $this->redirect(['view', 'id' => $id]);
                break;

            case Consts::FLOW_USER_DENIED:
                $model->{Consts::STATUS_ATTR} = Consts::FLOW_USER_DENIED;
                if ($model->validate()) {
                    $model->update();
                    return $this->redirect(['index']);
                }
                return $this->render('view', [
                    'model' => $model,
                ]);
                break;

            default:
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
                break;
        }
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        $images = NamesHelper::findImageNames($model->body);
        $dir_path = Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']) . Yii::$app->user->identity->username . '/' . $id;
        if (!empty($images)) {
            if (file_exists($dir_path)) {
                $dir_path_1 = Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']);
                foreach ($images as $image) {
                    if (file_exists($dir_path_1 . $image['img_src'])) {
                        unlink($dir_path_1 . $image['img_src']);
                    }
                }
                rmdir($dir_path);
            }
        } else {
            if (file_exists($dir_path)) {
                $files = scandir($dir_path, 1);
                array_pop($files);
                array_pop($files);
                if (!empty($files)) {
                    foreach ($files as $file) {
                        if (file_exists($dir_path.'/'.$file)) {
                            unlink($dir_path.'/'.$file);
                        }
                    }
                }
                rmdir($dir_path);
            }
        }

        if (isset($model->id)) {
            $model->delete();
        }

        Yii::$app->getSession()->setFlash('danger', 'Статья удалена!');
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
