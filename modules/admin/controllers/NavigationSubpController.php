<?php

namespace app\modules\admin\controllers;

use app\modules\admin\models\Navigation;
use Yii;
use app\modules\admin\models\NavigationSubp;
use app\modules\admin\models\NavigationSubpSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NavigationSubpController implements the CRUD actions for NavigationSubp model.
 */
class NavigationSubpController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all NavigationSubp models.
     * @return mixed
     */
    public function actionIndex($main_id)
    {
        $prev_nav_point = Navigation::findOne($main_id);                                            //получаем обьект пунктов меню предыдущего уровня
        $searchModel = new NavigationSubpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $main_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'prev_nav_point' => $prev_nav_point,
        ]);
    }

    /**
     * Displays a single NavigationSubp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $prev_nav_point = Navigation::findOne($model->nav_id);
        return $this->render('view', [
            'model' => $model,
            'prev_nav_point' => $prev_nav_point,
        ]);
    }

    /**
     * Creates a new NavigationSubp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($main_id)
    {
        $model = new NavigationSubp();
        $prev_nav_point = Navigation::findOne($main_id);
        $model->nav_id = $prev_nav_point->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'prev_nav_point' => $prev_nav_point,
        ]);
    }

    /**
     * Updates an existing NavigationSubp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $prev_nav_point = Navigation::findOne($model->nav_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'prev_nav_point' => $prev_nav_point,
        ]);
    }

    /**
     * Deletes an existing NavigationSubp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NavigationSubp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NavigationSubp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NavigationSubp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
