<?php

namespace app\modules\admin\controllers;

use app\modules\main\models\MainCaruselImages;
use Yii;
use yii\web\Controller;
use app\modules\admin\models\FilesUpload;
use yii\web\UploadedFile;

class MainCaruselController extends Controller
{
    public function actionIndex()
    {
        $images = MainCaruselImages::find()->all();
        return $this->render('index', [
            'images' => $images,
        ]);
    }

    public function actionAddPhotos()
    {
        $db_images = MainCaruselImages::find()->all();
        $images_files = new FilesUpload([
            'scenario' => FilesUpload::SCENARIO_UPDATE,
        ]);
        $file_type = 'images';                                                                                  // определяем тип заружаемых данных
        $path = __DIR__.'/../../../'.Yii::$app->params['main.carusel.photos'];
        $file_names = false;
        $preview = $images_files->initialPreviewGenerator($db_images, Yii::$app->params['main.carusel.photos']);

        if (Yii::$app->request->isPost) {
            $images_files->images = UploadedFile::getInstances($images_files, 'images');                        //загружаем фотку
            if (!empty($images_files->images)) {                                                                //если файлы загружены - сохраняем их, запись в бд идет в модели
                $file_names = $images_files->uploadThis($file_type, $path);                                     //сохраняем
                
                if (!$file_names) {
                    Yii::$app->getSession()->setFlash('danger', 'Данные не сохраняются на диск!');
                    return $this->redirect(['index']);
                }
                foreach ($file_names as $name) {
                    $image = MainCaruselImages::findOne(['img_src' => $name]);
                    if (!isset($image->img_src)) {
                        $image = new MainCaruselImages();
                        $image->img_src = $name;
                        $image->save();
                        unset($image);
                    }
                }
            }
            Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
            return $this->redirect(['index']);
        }

        return $this->render('add-photos', [
            'db_images' => $db_images,
            'preview' => $preview,
            'images_files' => $images_files,
        ]);
    }

    public function actionAddText($id)
    {
        $db_image = MainCaruselImages::findOne($id);
        
        if ($db_image->load(Yii::$app->request->post()) && $db_image->save()) {
            Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
            return $this->redirect(['index']);
        }
        
        return $this->render('add-text', [
            'db_image' => $db_image,
        ]);
    }

    public function actionDeleteFiles($img_id, $img_name)                         //экшн для удаления фоток из картик инпута
    {
        $path = __DIR__.'/../../../'.Yii::$app->params['main.carusel.photos'];
        unlink($path.$img_name);
        $model = MainCaruselImages::findOne($img_id);                                     //подчищаем названия файлов в базе, в обоих строках
        $model->delete();
        return '{}';                                                               //надо вернуть это, чтобы картик инпут не вываливался в ошибку
    }
}
