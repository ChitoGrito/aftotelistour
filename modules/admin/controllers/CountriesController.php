<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Countries;
use app\modules\admin\models\CountriesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\FilesUpload;
use yii\web\UploadedFile;

/**
 * CountriesController implements the CRUD actions for Countries model.
 */
class CountriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Countries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CountriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Countries model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Countries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Countries();
        $file_type = 'images';                                                                                  // определяем тип заружаемых данных
        $path = __DIR__.'/../../../'.Yii::$app->params['countries.icons'];
        $files_upload = new FilesUpload(['scenario' => FilesUpload::SCENARIO_NEW]);
        $files_upload->new_one = true;
        $preview = $files_upload->initialPreviewGenerator($model, Yii::$app->params['countries.icons']);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $files_upload->images = UploadedFile::getInstances($files_upload, 'images');
            $file_names = $files_upload->uploadThis($file_type, $path);                                     //сохраняем
            if (!$file_names) {
                Yii::$app->getSession()->setFlash('danger', 'Сохранение не прошло из-за картинки!');
                return $this->redirect(['index']);
            }
            if ($file_names) {
                foreach ($file_names as $name) {
                    $model->img_src = $name;                                                                    //пишу в img_src первого тоже, для корректной работы функции initialPreviewGenerator
                }
            }
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'files_upload' => $files_upload,
            'preview' => $preview,
        ]);
    }

    /**
     * Updates an existing Countries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $file_type = 'images';                                                                                  // определяем тип заружаемых данных
        $path = __DIR__.'/../../../'.Yii::$app->params['countries.icons'];
        if (!file_exists($path.$model->img_src)) {
            $files_upload = new FilesUpload([
                'scenario' => FilesUpload::SCENARIO_NEW,
            ]);
            $files_upload->new_one = true;
        } else {
            $files_upload = new FilesUpload([
                'scenario' => FilesUpload::SCENARIO_UPDATE,
            ]);
        }
        
        $preview = $files_upload->initialPreviewGenerator($model, Yii::$app->params['countries.icons']);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //if (!file_exists($path.))
            $files_upload->images = UploadedFile::getInstances($files_upload, 'images');
            $file_names = $files_upload->uploadThis($file_type, $path);                                     //сохраняем
            if ($file_names === false) {
                Yii::$app->getSession()->setFlash('danger', 'Сохранение не прошло из-за картинки!');
                return $this->redirect(['index']);
            }
            if ($file_names) {
                foreach ($file_names as $name) {
                    $model->img_src = $name;                                                                    //пишу в img_src первого тоже, для корректной работы функции initialPreviewGenerator
                }
            }
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'files_upload' => $files_upload,
            'preview' => $preview,
        ]);
    }

    /**
     * Deletes an existing Countries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $path = __DIR__.'/../../../'.Yii::$app->params['countries.icons'];
        if (file_exists($path.$id)) {
            unlink($path.$id);
        }
        return $this->redirect(['index']);
    }

    public function actionDeleteFiles($img_id, $img_name)                         //экшн для удаления фоток из картик инпута
    {
        $path = __DIR__.'/../../../'.Yii::$app->params['countries.icons'];
        unlink($path.$img_name);
        return '{}';                                                               //надо вернуть это, чтобы картик инпут не вываливался в ошибку
    }

    /**
     * Finds the Countries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Countries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Countries::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
