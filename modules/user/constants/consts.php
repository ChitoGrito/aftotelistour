<?php

namespace app\modules\user\constants;

class Consts
{
    const FLOW_NAME = 'PostWorkflow';
    const STATUS_ATTR = 'post_status';
    /* Статусы цикла статей
    называть статусы стоит по самому низшему вхождению роли
    */
    /* Для юзеров */
    const FLOW_NEW = 'PostWorkflow/new';
    const FLOW_USER_PREVIEW = 'PostWorkflow/UserPreview';
    const FLOW_USER_PROCESS = 'PostWorkflow/UserProcess';
    const FLOW_USER_READY = 'PostWorkflow/UserReady';
    const FLOW_USER_DENIED = 'PostWorkflow/AdminDenied';

    /* Общие */
    const FLOW_ARCHIVE = 'PostWorkflow/Archive';

    /* Для админа */
    const FLOW_ADMIN_CHECKING = 'PostWorkflow/AdminChecking';
    const FLOW_ADMIN_PROCESS = 'PostWorkflow/AdminProcess';
    const FLOW_ADMIN_PUBLISHED = 'PostWorkflow/AdminPublished';

    const ADMIN_ACESS_LEVELL = 9999;
    const EDITOR_ACESS_LEVELL = 5;
    const USER_ACESS_LEVELL = 1;
    /* ********************************************************* */
}
