<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\modules\main\components\widgets\Alert;
use app\modules\user\constants\Consts;

$this->title =  Html::encode('Создать статью');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = ['label' => 'Мои статьи', 'url' => ['my-posts/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- MAIN -->
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($model->country->name) ?></h1>
        </header>

        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
            <div class="line">
                <?php
                $nextSteps = $model->nextSteps();
                //debug($nextSteps);
                echo '<div class="margin-bottom">';
                foreach ($nextSteps as $step) {
                    /* if ($step['name'] == Consts::FLOW_USER_PREVIEW) {
                        $step['meta']['actionLabel'] = 'Извлечь из архива';
                    } */
                    echo Html::a(Html::encode($step['meta']['actionLabel']), ['get-action', 'id' => $model->id, 'to_status' => $step['name']], ['class' => 'button button-primary-stroke margin-right']);
                }
                echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'button cancel-btn',
                    'data' => [
                        'confirm' => 'Вы уверены, что хотите удалить эту статью?',
                        'method' => 'post',
                    ],
                ]);
                echo '</div>';
                ?>
            </div>
            <div class="line">
                <h2 class="text-size-30"><?= $model->main_theme ?></h2>
                <?= $model->body ?>
            </div>
        </div>
    </article>
</main>