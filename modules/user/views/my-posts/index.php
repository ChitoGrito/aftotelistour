<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use kartik\date\DatePicker;
use app\modules\main\components\widgets\Alert;
use app\modules\user\constants\Consts;

$this->title = 'Мои статьи';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line" style="padding-left: 0;">
                <div class="margin-bottom">
                    <?php
                    echo Html::a(Html::encode('Создать статью'), ['process'], ['class' => 'button background-primary border-radius text-white margin-right-40']);
                    echo Html::a(Html::encode('Архив'), ['archive'], ['class' => 'button button-primary-stroke border-radius']);
                    //debug($searchModel->allSteps());
                    ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'country_id',
                                'value' => 'country.name',
                                'filter' => ArrayHelper::map(Countries::find()->all(), 'id', 'name'),
                                'options' => ['width' => '150'],
                            ],

                            [
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'date_from_create',
                                    'attribute2' => 'date_to_create',
                                    'type' => DatePicker::TYPE_RANGE,
                                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                                    'pluginOptions' => [
                                        'format' => 'dd.mm.yyyy',
                                        'autoclose' => false,
                                    ],
                                    'options' => [
                                        // you can hide the input by setting the following
                                        'autocomplete' => 'off',
                                    ]
                                ]),
                                'attribute' => 'created_at',
                                'contentOptions' => ['style' => 'width: 25%;'],
                                'format' => ['date', 'php:d.m.Y H:i:s'],
                            ],

                            [
                                'attribute' => Consts::STATUS_ATTR,
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return '<span class="label '.$data->getWorkflowStatus()->getMetadata('labelClass').'">'.$data->getWorkflowStatus()->getLabel().'</span>';
                                },
                                'filter' => ArrayHelper::map(
                                    $searchModel->allSteps(
                                        $unset = array(
                                            Consts::FLOW_NEW,
                                            Consts::FLOW_ARCHIVE
                                        )
                                    ),
                                    'name',
                                    'label'
                                ),
                            ],

                            [
                                'attribute' => 'main_theme',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a(Html::encode($model->main_theme), ['view', 'id' => $model->id]);
                                }
                            ],

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
                                'template' => '{view} {delete}',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </article>
</main>