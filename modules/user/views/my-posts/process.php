<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use app\modules\main\components\widgets\Alert;

$this->title = 'Работа со статьёй';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = ['label' => 'Мои статьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1">
                <?php
                echo Html::encode($this->title)
                ?>
            </h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <?php Pjax::begin(['id' => 'new_note']) ?>
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
                <?php Pjax::end(); ?>

            </div>
        </div>
    </article>
</main>