<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use kartik\date\DatePicker;
use app\modules\main\components\widgets\Alert;

$this->title = 'Архив';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['profile/index']];
$this->params['breadcrumbs'][] = ['label' => 'Мои статьи', 'url' => ['my-posts/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            [
                                'attribute' => 'country_id',
                                'value' => 'country.name',
                                'filter' => ArrayHelper::map(Countries::find()->all(), 'id', 'name'),
                                'options' => ['width' => '150'],
                            ],

                            [
                                'filter' => DatePicker::widget([
                                    'model' => $searchModel,
                                    'attribute' => 'date_from_create',
                                    'attribute2' => 'date_to_create',
                                    'type' => DatePicker::TYPE_RANGE,
                                    'separator' => '<i class="glyphicon glyphicon-resize-horizontal"></i>',
                                    'pluginOptions' => [
                                        'format' => 'dd.mm.yyyy',
                                        'autoclose' => false,
                                    ],
                                    'options' => [
                                        // you can hide the input by setting the following
                                        'autocomplete' => 'off',
                                    ]
                                ]),
                                'attribute' => 'created_at',
                                'contentOptions' => ['style' => 'width: 25%;'],
                                'format' => ['date', 'php:d.m.Y H:i:s'],
                            ],

                            //'author_id',
                            //'show_it',
                            [
                                'attribute' => 'show_it',
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return $data->show_it ? '<span class="label label-success">Вкл</span>' : '<span class="label label-danger">Выкл</span>';
                                },
                                'filter' => array(
                                    0 => 'Выкл',
                                    1 => 'Вкл',
                                ),
                                //'options' => ['width' => '150'],
                            ],
                            //'main_theme',
                            [
                                'attribute' => 'main_theme',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a(Html::encode($model->main_theme), ['view', 'id' => $model->id]);
                                }
                            ],
                            //'sub_theme',
                            //'body:ntext',

                            [
                                'class' => 'yii\grid\ActionColumn',
                                'contentOptions' => ['style' => 'white-space: nowrap; text-align: center; letter-spacing: 0.1em; max-width: 7em;'],
                                'template' => '{view}',
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </article>
</main>