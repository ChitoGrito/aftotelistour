<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Countries;
use app\modules\user\constants\Consts;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use yii\helpers\BaseInflector;

?>

<div class="margin">
    <?php
    //Pjax::begin();
    $form = ActiveForm::begin([
        'id' => 'new-post-form',
        'options' => [
            //'class' => 'form-horizontal',
            'data-pjax' => true,
        ],
    ]);
    $countries = ArrayHelper::map(Countries::find()->all(), 'id', 'name');
    $params = [
        'prompt' => 'Выберите страну...',
        'class' => 'form-control',
    ];
    echo $form->errorSummary($model);
    ?>

    <?= $form->field($model, 'country_id', ['inputOptions' => ['autocomplete' => 'off']])->dropDownList($countries, $params)->label('Выберите страну') ?>

    <?= $form->field($model, 'main_theme', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['maxlength' => true, 'class' => 'form-control']) ?>

    <?= $form->field($model, 'sub_theme', ['inputOptions' => ['autocomplete' => 'off']])->textarea(['rows' => 3, 'class' => 'form-control']) ?>



    <?php
    echo $form->field($model, 'body')->widget(Widget::className(), [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['my-posts/image-upload', 'id' => $model->id]),
            'imageManagerJson' => Url::to(['my-posts/images-get', 'id' => $model->id]),
            'fileUpload' => Url::to(['my-posts/file-upload', 'id' => $model->id]),
            'fileManagerJson' => Url::to(['my-posts/files-get', 'id' => $model->id]),
            'imageDelete' => Url::to(['my-posts/image-delete', 'id' => $model->id]),
            'fileDelete' => Url::to(['my-posts/file-delete', 'id' => $model->id]),
            'plugins' => [
                'fullscreen',
            ],
        ],
        'plugins' => [
            'filemanager' => 'vova07\imperavi\bundles\FileManagerAsset',
            'imagemanager' => 'vova07\imperavi\bundles\ImageManagerAsset',

        ],
    ]);
    //echo Html::error($model, 'body', ['class' => 'daterror']);
    ?>

    <div class="form-group">
        <?php
        $nextSteps = $model->nextSteps();
        //debug($model->fromArchive);
        foreach ($nextSteps as $step) {
            if ($step['name'] == Consts::FLOW_ARCHIVE) {
                $step['meta']['actionLabel'] = 'Сохранить и положить в архив';
            }
            echo Html::submitButton(Html::encode($step['meta']['actionLabel']), ['class' => 'submit-form button background-primary border-radius text-white margin-right-30']);
        }
        ?>
    </div>

    <?php
    ActiveForm::end();
    ?>
</div>