<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\user\models\LoginForm */

$this->title = Yii::t('app', 'TITLE_LOGIN');
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <h2 class="text-thin margin-bottom-30 margin-top">Логин</h2>
                    <?= $form->field($model, 'username', ['inputOptions' => ['autocomplete' => 'off']])->label(false) ?>
                    <h2 class="text-thin margin-bottom-30 margin-top">Пароль</h2>
                    <?= $form->field($model, 'password', ['inputOptions' => ['autocomplete' => 'off']])->passwordInput()->label(false) ?>
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                    <div style="color:#999;margin:1em 0">
                        <?= Yii::t('app', 'IF_FORGOR_PASSWORD') ?> <?= Html::a(Yii::t('app', 'RESET_IT'), ['password-reset-request']) ?>.
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'BUTTON_LOGIN'), ['class' => 'button background-primary border-radius text-white', 'name' => 'login-button']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>