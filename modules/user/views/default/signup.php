<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use kartik\file\FileInput;

$this->title = Yii::t('app', 'TITLE_SIGNUP');
$this->params['breadcrumbs'][] = $this->title;
?>

<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?php
                    $form = ActiveForm::begin();
                    echo $form->errorSummary($model);
                    ?>
                    <div class="form-group s-12 m-12 l-6 padding-right">
                        <h2 class="text-thin margin-bottom-30 margin-top">Введите логин:</h2>
                        <?= $form->field($model, 'username', [
                            'inputOptions' => ['autocomplete' => 'off'],
                        ])->textInput([
                            'placeholder' => 'Логин',
                            'title' => 'Логин',
                        ])->label(false) ?>



                        <h2 class="text-thin margin-bottom-30 margin-top">Введите вашу электронную почту:</h2>
                        <?= $form->field($model, 'email', [
                            'inputOptions' => ['autocomplete' => 'off'],
                        ])->textInput([
                            'placeholder' => 'E-mail',
                            'title' => 'E-mail',
                        ])->label(false) ?>


                        <h2 class="text-thin margin-bottom-30 margin-top">Введите пароль:</h2>
                        <?= $form->field($model, 'password', [
                            'inputOptions' => ['autocomplete' => 'off'],
                        ])->passwordInput([
                            'placeholder' => 'Пароль',
                            'title' => 'Пароль',
                        ])->label(false) ?>


                        <h2 class="text-thin margin-bottom-30 margin-top">Докажите, что вы не робот!</h2>
                        <?= $form->field($model, 'verifyCode', [
                            'inputOptions' => ['autocomplete' => 'off'],
                        ])->widget(Captcha::className(), [
                            'captchaAction' => '/user/default/captcha',
                            'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                        ])->label(false) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('app', 'BUTTON_SIGNUP'), ['class' => 'button background-primary border-radius text-white']) ?>
                        </div>
                    </div>

                    <div class="form-group s-12 m-12 l-6">
                        <h2 class="text-thin margin-bottom-30 margin-top">Выберете фотографию профиля:</h2>
                        <?php
                        echo $form->field($files_upload, 'images')->widget(FileInput::classname(), [
                            /* 'model' => $files_upload,
                                'attribute' => 'images', */
                            'options' => ['multiple' => true, 'accept' => 'image/*'],
                            'pluginOptions' => [
                                'initialPreview' => $preview['for_preview'],
                                'initialPreviewConfig' => $preview['initialPreviewConfig'],
                                'initialPreviewAsData' => true,
                                'overwriteInitial' => false,
                                'maxFileSize' => 8800,
                                'previewFileType' => 'any',
                                //'maxFileCount' => 1,
                                /* 'showCaption' => false,
                                'showRemove' => false,
                                'showUpload' => false,
                                'browseClass' => 'btn btn-primary btn-block',
                                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                                'browseLabel' =>  'Выбрать фото', */
                            ]
                        ])->label(false);
                        ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>