<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('app', 'TITLE_RESET_PASSWORD');
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <h2 class="text-thin margin-bottom-30 margin-top">Новый пароль</h2>
                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label(false) ?>
                    <h2 class="text-thin margin-bottom-30 margin-top">Повторите новый пароль</h2>
                    <?= $form->field($model, 'passwordRepeat')->passwordInput(['autofocus' => true])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'BUTTON_SAVE'), ['class' => 'button background-primary border-radius text-white']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>