<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('app', 'TITLE_REQUEST_RESET_PASSWORD');
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
                    <h2 class="text-thin margin-bottom-30 margin-top"><?= Yii::t('app', 'FILL_YOUR_EMAIL') ?></h2>
                    <?= $form->field($model, 'email', ['inputOptions' => ['autocomplete' => 'off']])->textInput(['autofocus' => true])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'BUTTON_SEND'), ['class' => 'button background-primary border-radius text-white']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>