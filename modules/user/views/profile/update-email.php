<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = 'Изменение адреса электронной почты';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <h2 class="text-uppercase text-strong margin-bottom-30">Ваш адрес электронной почты:</h2>
                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'class' => 'customform',
                        ],
                    ]); ?>

                    <?=$form->field($model, 'email', [
                        'inputOptions' => ['autocomplete' => 'off'],
                        'options' => [
                            'tag' => false,
                            'class' => 's-12 m-12 l-6',
                        ],
                    ])->textInput([
                        'class' => 'subject border-radius',
                        'placeholder' => 'Ваш e-mail',
                        'title' => 'Ваш e-mail',
                    ])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'BUTTON_SAVE'), ['class' => 'submit-form button background-primary border-radius text-white']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </article>
</main>