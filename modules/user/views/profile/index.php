<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Breadcrumbs;
use app\modules\main\components\widgets\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\user\models\User */

$this->title = Yii::t('app', 'TITLE_PROFILE');
$this->params['breadcrumbs'][] = $this->title;
?>

<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin">
                    <?= Alert::widget([
                        'options' => [
                            'class' => 'footer-alert text-thin text-white',
                        ],
                    ]) ?>
                </div>
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">

                    <div class="s-12 m-12 l-6">

                        <div class="s-12 m-12 l-5 center margin-bottom-40">
                            <div class="image-border-radius">
                                <?= Html::img('@' . Yii::$app->params['users.profile.photos'] . Yii::$app->user->identity->user_pic, []) ?>
                            </div>
                        </div>

                        <div class="block-bordered">
                            <?= DetailView::widget([
                                'model' => $model,
                                'options' => [
                                    'class' => 'table',
                                ],
                                'attributes' => [
                                    'username',
                                    'email',
                                ],
                            ]) ?>
                        </div>

                    </div>

                    <div class="s-12 m-12 l-6">
                        <div class="padding">
                            <div class="float-left">
                                <?= Html::a('<i class="icon-mail background-primary icon-circle-small text-size-20"></i>', ['update-email']) ?>
                            </div>
                            <div class="margin-left-80 margin-top-10">
                                <?= Html::a('Изменить адрес эл. почты', ['update-email'], [
                                    'class' => 'button button-primary-stroke'
                                ]) ?>
                            </div>
                        </div>

                        <div class="padding">
                            <div class="float-left">
                                <?= Html::a('<i class="icon-lock_alt background-primary icon-circle-small text-size-20"></i>', ['password-change']) ?>
                            </div>
                            <div class="margin-left-80 margin-top-10">
                                <?= Html::a(Yii::t('app', 'LINK_PASSWORD_CHANGE'), ['password-change'], [
                                    'class' => 'button button-primary-stroke'
                                ]) ?>
                            </div>
                        </div>

                        <div class="padding">
                            <div class="float-left">
                                <?= Html::a('<i class="icon-camera background-primary icon-circle-small text-size-20"></i>', ['pic-change']) ?>
                            </div>
                            <div class="margin-left-80 margin-top-10">
                                <?= Html::a('Изменить фото профиля', ['pic-change'], [
                                    'class' => 'button button-primary-stroke'
                                ]) ?>
                            </div>
                        </div>

                        <div class="padding">
                            <div class="float-left">
                                <?= Html::a('<i class="icon-newspaper background-primary icon-circle-small text-size-20"></i>', ['my-posts/index']) ?>
                            </div>
                            <div class="margin-left-80 margin-top-10">
                                <?= Html::a('Мои статьи', ['my-posts/index'], [
                                    'class' => 'button button-primary-stroke'
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
</main>