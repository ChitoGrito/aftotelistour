<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\widgets\Breadcrumbs;

$this->title = 'Изменить фото профиля';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'TITLE_PROFILE'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<main role="main">
    <!-- Content -->
    <article>
        <header class="section background-primary text-center">
            <h1 class="text-white margin-bottom-0 text-size-50 text-thin text-line-height-1"><?= Html::encode($this->title) ?></h1>
        </header>
        <div class="section background-white">
            <div class="line">
                <div class="margin margin-bottom-40">
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                </div>
            </div>
            <div class="line">
                <div class="margin">
                    <h2 class="text-uppercase text-strong margin-bottom-30">Фото профиля:</h2>
                    <?php
                    $form = ActiveForm::begin();
                    echo $form->field($files_upload, 'images')->widget(FileInput::classname(), [
                        'options' => ['multiple' => true, 'accept' => 'image/*'],
                        'pluginOptions' => [
                            'initialPreview' => $preview['for_preview'],
                            'initialPreviewConfig' => $preview['initialPreviewConfig'],
                            'initialPreviewAsData' => true,
                            'overwriteInitial' => false,
                            'maxFileSize' => 8800,
                            'previewFileType' => 'any',
                            'maxFileCount' => 1,
                            /* 'showCaption' => false,
                            'showRemove' => false,
                            'showUpload' => false,
                            'browseClass' => 'btn btn-primary btn-block',
                            'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                            'browseLabel' =>  'Выбрать фото', */
                        ]
                    ]);
                    ?>

                    <div class="customform">
                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'submit-form button background-primary border-radius text-white']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

            </div>
    </article>
</main>