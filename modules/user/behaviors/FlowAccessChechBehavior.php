<?php

namespace app\modules\user\behaviors;

use yii\base\Behavior;
use app\modules\user\constants\Consts;
use Yii;

class FlowAccessChechBehavior extends Behavior
{
    public $locationFront;

    /* Функуия возвращает массив возможных переходов (с лавел и метаданнами) с текущего статуса в воркфлоу,
    *  учитывая разрешения в РБАК и расположение вызова (у юзера, или в админке)
    */
    public function nextSteps()
    {
        if (!isset($this->locationFront)) {
            throw new \yii\web\HttpException(500, 'Задай фронт, или бэк для поведения ёпт!!!');       // проверяю, объявлена ли эта переменная, тк это важно
        }

        if ($this->owner->hasWorkflowStatus()) {
            $nextSteps = [];
            $thisStatus = $this->owner->getWorkflowStatus()->getId();                                   // текущий статус
            // let's ask the Status object then
            $transitions = $this->owner->getWorkflowStatus()->getTransitions();                         // следующие возможные статусы
            $num = 0;
            foreach ($transitions as $transition) {
                $nextStatus = $transition->getEndStatus()->getId();

                if (\Yii::$app->user->can($thisStatus . '->' . $nextStatus)) {                          // проверяю, имеет ли пользак разрешения на сие деяние
                    $nextLabel = $transition->getEndStatus()->getLabel();
                    $nextMetadata = $transition->getEndStatus()->getMetadata();
                    if ($this->locationFront) {
                        if (stripos($nextStatus, 'admin')) {                                            // определение, в какой части сайта происходит деяние - в админке или у пользаков
                            continue;
                        }
                        $nextSteps[$num]['name'] = $nextStatus;
                        $nextSteps[$num]['label'] = $nextLabel;
                        $nextSteps[$num]['meta'] = $nextMetadata;
                    } else {
                        if (stripos($nextStatus, 'user')) {
                            continue;
                        }
                        $nextSteps[$num]['name'] = $nextStatus;
                        $nextSteps[$num]['label'] = $nextLabel;
                        $nextSteps[$num]['meta'] = $nextMetadata;
                    }
                    $num++;
                }
            }
            return $nextSteps;
        } else {
            throw new \yii\web\HttpException(500, 'Модель не в WorkFlow!');
        }
    }

    public function allSteps($unset)
    {
        $allStatuses = [];
        $statuses = Yii::$app->PostWorkflowSource->getAllStatuses(Consts::FLOW_NAME);
        $num = 0;
        foreach ($statuses as $status) {
            if (!in_array($status->getId(), $unset)) {
                $allStatuses[$num]['name'] = $status->getId();
                $allStatuses[$num]['label'] = $status->getLabel();
                $allStatuses[$num]['meta'] = $status->getMetadata();
                $num++;
            }
        }
        return $allStatuses;
    }

    public function getLocationFront()
    {
        return $this->locationFront;
    }

    public function setLocationFront($value)
    {
        $this->locationFront = $value;
    }

    public function checkAccess($name)
    {
        $nextSteps = $this->nextSteps();
        $ok = false;
        foreach ($nextSteps as $step) {
            if ($step['name'] == $name) {
                $ok = true;
            }
        }
        return $ok;
    }
}
