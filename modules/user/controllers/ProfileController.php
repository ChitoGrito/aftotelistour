<?php

namespace app\modules\user\controllers;

use app\modules\user\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use app\modules\user\forms\ProfileUpdateForm;
use app\modules\user\forms\PasswordChangeForm;
use app\modules\admin\models\FilesUpload;
use yii\web\UploadedFile;

class ProfileController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index', [
            'model' => $this->findModel(),
        ]);
    }

    public function actionUpdateEmail()
    {
        $user = $this->findModel();
        $model = new ProfileUpdateForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->getSession()->setFlash('success', 'Новый Email сохранен!');
            return $this->redirect(['index']);
        } else {
            return $this->render('update-email', [
                'model' => $model,
            ]);
        }
    }

    public function actionPasswordChange()
    {
        $user = $this->findModel();
        $model = new PasswordChangeForm($user);

        if ($model->load(Yii::$app->request->post()) && $model->changePassword()) {
            Yii::$app->getSession()->setFlash('success', 'Новый пароль сохранен успешно!');
            return $this->redirect(['index']);
        } else {
            return $this->render('passwordChange', [
                'model' => $model,
            ]);
        }
    }

    public function actionPicChange()
    {
        $user = $this->findModel();
        $files_upload = new FilesUpload([
            'scenario' => FilesUpload::SCENARIO_UPDATE,
        ]);
        $file_type = 'images';                                                                                  // определяем тип заружаемых данных
        $path = '/var/www/start2/docs/'.Yii::$app->params['users.profile.photos'];
        $file_names = false;
        $preview = $files_upload->initialPreviewGenerator($user, Yii::$app->params['users.profile.photos'], 'user_pic');

        if (Yii::$app->request->isPost) {
            $files_upload->images = UploadedFile::getInstances($files_upload, 'images');
            if (!empty($files_upload->images)) {                                                                //если файлы загружены - сохраняем их, запись в бд идет в модели
                if ($user->user_pic != null and file_exists($path.$user->user_pic)) {
                    unlink($path.$user->user_pic);                                                                     // на всякий подчищаем всю папку, нужна только одна текущая фотка
                }
                $file_names = $files_upload->uploadThis($file_type, $path);                                     //сохраняем
                if (!$file_names) {
                    Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены из-за фотографии!');
                    return $this->redirect(['index']);
                }
            }
            if ($file_names) {
                foreach ($file_names as $name) {
                    $user->user_pic = $name;
                }
            }
            if ($user->save()) {
                Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                return $this->redirect(['index']);
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                return $this->redirect(['index']);
            }
        }

        return $this->render('pic-change', [
            //'model' => $model,
            'files_upload' => $files_upload,
            'preview' => $preview,
        ]);
    }

    public function actionDeleteFiles($img_id, $img_name)                         //экшн для удаления фоток из картик инпута
    {
        $path = __DIR__.'/../../../'.Yii::$app->params['users.profile.photos'];
        $user = $this->findModel();                                    //подчищаем названия файлов в базе, в обоих строках
        if (file_exists($path.$user->user_pic)) {
            unlink($path.$user->user_pic);
            $user->user_pic = null;
            $user->save();
        }
        return '{}';                                                               //надо вернуть это, чтобы картик инпут не вываливался в ошибку
    }

    /**
     * @return User the loaded model
     */
    private function findModel()
    {
        return User::findOne(Yii::$app->user->identity->getId());
    }
}
