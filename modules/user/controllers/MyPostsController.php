<?php

namespace app\modules\user\controllers;

use app\modules\main\models\Posts;
use app\modules\user\constants\Consts;
use app\modules\user\models\MyPostsSearch;
use yii\web\Controller;
use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\main\components\helpers\NamesHelper;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

class MyPostsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actions()               // пути к расположению файлов прописал в самом модуле расширения, тут чисто чтобы не валилось в ошибку
    {
        return [
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
                'translit' => true,
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.jpg', '*.jpeg', '*.png', '*.gif', '*.ico']], // These options are by default.
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetFilesAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
                'options' => ['only' => ['*.txt', '*.md', '*.pdf', '*.doc', '*.docx', '*.zip', '*.rar', '*.7z']], // These options are by default.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false, // For any kind of files uploading.
                //'unique' => false,
                'translit' => true,
            ],
            'image-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.photos']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']), // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => Url::to(['@web' . Yii::$app->params['posts.files']]), // Directory URL address, where files are stored.
                'path' => Yii::getAlias('@webroot' . Yii::$app->params['posts.files']), // Or absolute path to directory where files are stored.
            ],
        ];
    }

    public function actionIndex()                           // отоброжения всех статей автора (юзера)
    {
        $searchModel = new MyPostsSearch([
            'isArchive' => false,                           // чтобы исключить записи в архиве
            'locationFront' => true,                        // для корректной работы повдение, проверки следующих статусов и тд
        ]);
        //debug($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionArchive()                     // отображение записей, лежащих в архиве
    {
        $searchModel = new MyPostsSearch([
            'isArchive' => true,                        // чтобы исключить все статьи, кроме тех, что со статусом "архив"
            'locationFront' => true,                    // для работы поведение, указываем, что это сторона юзеров
        ]);
        //debug($searchModel);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('archive', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionProcess($id = false)
    {
        if ($id) {                                                           // понимаем, это новая новость, или редактирование
            $model = $this->findModel($id);
            $model->locationFront = true;

            if (!\Yii::$app->user->can('updateOwnPosts', ['post' => $model])) {
                throw new ForbiddenHttpException('Доступ закрыт! Вы не являетесь автором статьи!');
            }
        } else {
            $model = new Posts(['locationFront' => true]);                   // задается место создания обьекта, в данном случае - фронтэнд, тобишь юзер
            $model->enterWorkflow();                                         // входим в воркфлоу, начальный статус
            $model->show_it = true;                                          // флаг "удаленности" статьи, тоесть она не удалена
            $model->saveIt();                                                // создание обьекта этой статьи в БД
            return $this->redirect(['process', 'id' => $model->id]);         // переадресация на этот же экшн, чтобы исключить создание кучи лишних обьектов статей при обновлениях страницы, и просто так
        }

        if ($model->getWorkflowStatus()->getId() !== Consts::FLOW_USER_PROCESS) {       // если по какой-то причине у статьи статус не "корректирование"
            if ($model->checkAccess(Consts::FLOW_USER_PROCESS)) {                       // проверяем, возможен ли переход с текущего статуса на корректирование
                $model->sendToStatus(Consts::FLOW_USER_PROCESS);                        // переводи на этот статус
                $model->update();                                                       // обновляем данные в БД
            } else {
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
            }
        }

        if ($model->load(Yii::$app->request->post())) {                                  // загружаем данные из формы
            if ($model->checkAccess(Consts::FLOW_USER_PREVIEW)) {                        // проверяем, возможен ли переход на статус "Предпросмотр"
                $model->post_status = Consts::FLOW_USER_PREVIEW;                         // присваиваю статус обьтекту статьи таким образом, чтобы сработала валидация (валидация при переходе на определенный статус)
            } else {
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
            }
            if ($model->validate()) {                                                    // валидация
                if ($model->update()) {                                                  // обновление обьекта в БД
                    Yii::$app->getSession()->setFlash('success', 'Данные сохранены!');
                    return $this->redirect(['view', 'id' => $model->id]);
                } else {
                    Yii::$app->getSession()->setFlash('danger', 'Данные не сохранены!');
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('process', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);                                                 // находим обьект статьи в БД
        $model->locationFront = true;                                                   // задаем положение обьекта (у юзеров)

        if ($model->getWorkflowStatus()->getId() == Consts::FLOW_USER_PROCESS) {        // если у обьекта статус "корректировка" переводим страницу на экшн процесса и заставляем юзера жмакнуть кнопочку сохранить
            Yii::$app->getSession()->setFlash('danger', 'Заполните все поля, и сохранитесь, иначе предпросмотр не возможен!');
            return $this->redirect(['process', 'id' => $id]);
        }

        if (empty($model->country_id) or empty($model->main_theme) or empty($model->sub_theme) or empty($model->body)) {        // проверяем, заполнены ли ключевые поля, чтобы вьюшка не ломалась
            Yii::$app->getSession()->setFlash('danger', 'Заполните все поля, иначе предпросмотр не возможен!');
            return $this->redirect(['process', 'id' => $id]);                                                                   // если нет, заставляем заполнить
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionGetAction($id, $to_status)                                                    // экшн распределения по статусам
    {
        $model = $this->findModel($id);                                                                 // находим нужную модель

        if (!\Yii::$app->user->can('updateOwnPosts', ['post' => $model])) {                             // проверяем, является ли юзер автором этой статьи (ато вдруг умник попробуем изменять чужую статью)
            throw new ForbiddenHttpException('Доступ закрыт! Вы не являетесь автором статьи!');
        }

        $model->locationFront = true;                                                                   // для поведения и проверки следующих статусов

        if (!$model->checkAccess($to_status)) {                                                         // проверяем возможен ли переход на пришедший статус
            throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');
        }

        switch ($to_status) {                                                                           // свитч действий для пришедшего статуса
            case Consts::FLOW_USER_PROCESS:
                return $this->redirect(['process', 'id' => $id]);                                       // процесс перенаправляем в процесс
                break;

            case Consts::FLOW_ARCHIVE:                                                                  // архив
                $model->sendToStatus(Consts::FLOW_ARCHIVE);                                             // прикрепляем статус "архив"
                $model->update();                                                                       // обновляем БД
                return $this->redirect(['archive']);                                                    // перенаправляем на экшн просмотра всех статей в архиве
                break;

            case Consts::FLOW_USER_READY:                                                               // передано админку для проверки на публикацию
                $model->sendToStatus(Consts::FLOW_USER_READY);                                          // присваиваем статье этот статус
                $model->update();                                                                       // обновляем объект в БД
                return $this->redirect(['index']);                                                      // перенаправляем на экшн просмотра всех статей "в процессе"
                break;

            case Consts::FLOW_USER_PREVIEW:                                                             // превью юзера
                $model->sendToStatus(Consts::FLOW_USER_PREVIEW);                                        // присваиаем статус
                $model->update();                                                                       // обновляем объект в БД
                return $this->redirect(['view', 'id' => $id]);                                          // перенаправляем на экшн вьюшки
                break;

            default:
                throw new ForbiddenHttpException('Ошибка работы с воркфлоу!');                          // на всякий еще раз... если пришел левый статус выдаем ошибку
                break;
        }
    }

    public function actionDelete($id)                                                                   // удаление статьи (совсем, со всеми фотками нафиг вычищается все)
    {
        $model = $this->findModel($id);                                                                 // находим модель
        if (!\Yii::$app->user->can('updateOwnPosts', ['post' => $model])) {                             // проверяем, является ли юзер автором статьи, чтобы чужое не поудалял при желании и умении
            throw new ForbiddenHttpException('Доступ закрыт! Вы не являетесь автором статьи!');
        }

        if (!$model->show_it) {
            throw new ForbiddenHttpException('Данная статья удалена');                                  // проверяем не сделал ли пользак статью удаленной
        }

        /* $images = NamesHelper::findImageNames($model->body);                                            // из тела статьи вычленяем названия файлов фоток и тд
        $dir_path = Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']) . Yii::$app->user->identity->username . '/' . $id;    // место положение всех файлов по статье
        if (!empty($images)) {                                                                          // если раннее были найдены файлы в теле статьи
            if (file_exists($dir_path)) {                                                               // и если папка такая существует
                $dir_path_1 = Yii::getAlias('@webroot' . Yii::$app->params['posts.photos']);            // тут прикол в том, что оказывается название выычлененное из тела имеет вид "username/post_id/file_name"
                foreach ($images as $image) {                                                           // для каждого файлика
                    if (file_exists($dir_path_1 . $image['img_src'])) {                                 // если файлик существует по указанному пути
                        unlink($dir_path_1 . $image['img_src']);                                        // удаляем его нафиг
                    }
                }
                rmdir($dir_path);                                                                       // и удаляем саму папку туда-же нафиг
            }
        } else {
            if (file_exists($dir_path)) {                                                               // вдруг фоток нет, но папка создается по событию создания новой статьи
                rmdir($dir_path);                                                                       // поэтому надо ее удалить
            }
        } */

        if (isset($model->id)) {                                                                        // помечаем типа удаленной, чтобы совсем не удалилось
            $model->show_it = false;
            $model->update();
        }

        Yii::$app->getSession()->setFlash('danger', 'Статья удалена!');
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Posts::find()->where(['id' => $id, 'show_it' => true])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Такой статьи не существует!');
    }
}
