<?php

namespace app\modules\user\models\workflows;

use Yii;
use app\modules\user\constants\Consts;

class PostWorkflow implements \raoul2000\workflow\source\file\IWorkflowDefinitionProvider
{
    public function getDefinition()
    {
        return [
            'initialStatusId' => 'new',
            'status' => [
                /* Начальный статус статьи, "новая статья".
                   Имеет переход к статусу "процесса" у юзера или админа */
                Consts::FLOW_NEW => [
                    'label' => 'Новая статья',
                    'transition' => [Consts::FLOW_USER_PROCESS, Consts::FLOW_ADMIN_PROCESS],
                    'metadata' => [
                        'access' => 'user',                                                                      // роль доступа к этому статусу, указывать самую младшую, остальные наследуются
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,                                                                // уровень доступа - самый младший у юзера, самый высокий у админа
                        'actionLabel' => 'Создать новую статью',
                        'labelClass' => 'label-default',
                    ],
                ],

                /* Статус "процесс" (идет работа над статьей), переход обратно на предпросмотр */
                Consts::FLOW_USER_PROCESS => [
                    'label' => 'Корректировка пользователем',
                    'transition' => [Consts::FLOW_USER_PREVIEW],
                    'metadata' => [
                        'access' => 'user',
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,
                        'actionLabel' => 'Корректировать',
                        'labelClass' => 'label-default',
                    ],
                ],

                /* Статус "предпросмотр", переходы на архив, в работе, и готово к публикации */
                Consts::FLOW_USER_PREVIEW => [
                    'label' => 'Предпросмотр пользователя',
                    'transition' => [Consts::FLOW_USER_PROCESS, Consts::FLOW_ARCHIVE, Consts::FLOW_USER_READY],
                    'metadata' => [
                        'access' => 'user',
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,
                        'actionLabel' => 'Сохранить и/или совершить предпросмотр',
                        'labelClass' => 'label-primary',
                    ],
                ],


                /* Архив, переходы на предпросмотр юзера, корректировку юзером, на статус готово к публикации (юзер хочет, чтобы статью опубликовали), предпросмотр и корректировка админом */
                Consts::FLOW_ARCHIVE => [
                    'label' => 'Архив',
                    'transition' => [Consts::FLOW_USER_PREVIEW, Consts::FLOW_ADMIN_CHECKING],
                    'metadata' => [
                        'access' => 'user',
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,
                        'actionLabel' => 'Отозвать и положить в архив',
                        'labelClass' => 'label-default',
                    ],
                ],

                /* Готово к публикации (юзер хочет опубликоваться), переходы на архив и проверку админом */
                Consts::FLOW_USER_READY => [
                    'label' => 'К публикации',
                    'transition' => [Consts::FLOW_ARCHIVE, Consts::FLOW_ADMIN_CHECKING],
                    'metadata' => [
                        'access' => 'user',
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,
                        'actionLabel' => 'Предложить к публикации',
                        'labelClass' => 'label-warning',
                    ],
                ],

                /* Проверка админом, переход на редактирвоание админом, возврат пользаку и публицированно */
                Consts::FLOW_ADMIN_CHECKING => [
                    'label' => 'Проверка администратором',
                    'transition' => [Consts::FLOW_USER_DENIED, Consts::FLOW_ADMIN_PROCESS, Consts::FLOW_ADMIN_PUBLISHED, Consts::FLOW_ARCHIVE],
                    'metadata' => [
                        'access' => 'editor',
                        'accessLevel' => (int) Consts::EDITOR_ACESS_LEVELL,
                        'actionLabel' => 'Сохранить и/или выполнить предпросмотр администратора',
                        'labelClass' => 'label-info',
                    ],
                ],

                /* Возврат (отказ) пользаку, переходы обратно на проверку админом, архив, редактирование пользаком и просмотр пользаком */
                Consts::FLOW_USER_DENIED => [
                    'label' => 'Возвращено пользователю',
                    'transition' => [Consts::FLOW_ADMIN_CHECKING, Consts::FLOW_ARCHIVE, Consts::FLOW_USER_PREVIEW, Consts::FLOW_USER_PROCESS],
                    'metadata' => [
                        'access' => 'user',
                        'accessLevel' => (int) Consts::USER_ACESS_LEVELL,
                        'actionLabel' => 'Отклонить и вернуть пользователю',
                        'labelClass' => 'label-danger',
                    ],
                ],

                /* Редактирование админом, переход обратно на проверку админом (предпросмотр админа) */
                Consts::FLOW_ADMIN_PROCESS => [
                    'label' => 'Редактирование администратором',
                    'transition' => [Consts::FLOW_ADMIN_CHECKING],
                    'metadata' => [
                        'access' => 'editor',
                        'accessLevel' => (int) Consts::EDITOR_ACESS_LEVELL,
                        'actionLabel' => 'Корректировка администратором',
                        'labelClass' => 'label-info',
                    ],
                ],

                /* Статья публицирована, отображается на сайте. Переход в архив */
                Consts::FLOW_ADMIN_PUBLISHED => [
                    'label' => 'Публицированно',
                    'transition' => [Consts::FLOW_ARCHIVE],
                    'metadata' => [
                        'access' => 'editor',
                        'accessLevel' => (int) Consts::EDITOR_ACESS_LEVELL,
                        'actionLabel' => 'Публицировать',
                        'labelClass' => 'label-success',
                    ],
                ],
            ],
        ];
    }
}
