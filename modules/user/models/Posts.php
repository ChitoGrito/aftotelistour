<?php

namespace app\modules\user\models;

use Yii;
use app\modules\user\models\User;
use app\modules\admin\models\Countries;
use app\modules\user\behaviors\FlowAccessChechBehavior;
use app\modules\user\models\PostsLikes;
use app\modules\user\models\PostsViews;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%posts}}".
 *
 * @property int $id
 * @property int $country_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $upload_at
 * @property int $author_id
 * @property int|null $show_it
 * @property int|null $enable_likes
 * @property int|null $enable_dislikes
 * @property int|null $enable_coments
 * @property string $main_theme
 * @property string $sub_theme
 * @property string|null $body
 *
 * @property User $author
 * @property Countries $country
 */
class Posts extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),                                 // занесение временной метки создания/изменения пользователя
            
            [
                'class' => \raoul2000\workflow\base\SimpleWorkflowBehavior::className(),
                'statusAttribute' => 'post_status',                         // model attribute to store status
                'source' => 'PostWorkflowSource',                           // workflow source component name
                'defaultWorkflowId' => 'PostWorkflow',
            ],

            [
                'class' => FlowAccessChechBehavior::className(),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%posts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'upload_at', 'main_theme', 'sub_theme', 'body', 'show_it'], 'required'],
            [['country_id', 'author_id'], 'integer'],
            [['enable_likes', 'enable_dislikes', 'enable_coments', 'show_it'], 'boolean'],
            [['body'], 'string'],
            [['main_theme', 'sub_theme'], 'string', 'max' => 255],
            [['main_theme', 'sub_theme'], 'filter', 'filter' => 'trim'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => 'Страна',
            'created_at' => 'Время создания',
            'updated_at' => 'Время обновления',
            'upload_at' => 'Время публикации',
            'author_id' => Yii::t('app', 'Author ID'),
            'show_it' => 'Отображение',
            'main_theme' => 'Основная тема',
            'sub_theme' => 'Описание',
            'body' => 'Содержание (текст)',
            'enable_likes' => 'Активировать лайки',
            'enable_dislikes' => 'Активировать дизлайки',
            'enable_coments' => 'Активировать комментарии',
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id']);
    }

    public function getLikes()
    {
        return $this->hasMany(PostsLikes::className(), ['post_id' => 'id']);
    }

    public function getViews()
    {
        return $this->hasMany(PostsViews::className(), ['post_id' => 'id']);
    }

    public function saveIt($model)
    {
        $model->author_id = Yii::$app->user->identity->id;
        $model->upload_at = strtotime($model->upload_at);
        if ($model->save()) {
            return true;
        } else {
            return false;
        }
    }
}
