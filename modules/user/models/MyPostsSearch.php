<?php

namespace app\modules\user\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\main\models\Posts;
use app\modules\user\constants\Consts;
use Yii;

/**
 * PostsSearch represents the model behind the search form of `app\modules\main\models\Posts`.
 */
class MyPostsSearch extends Posts
{
    public $isArchive;

    public $date_from_upload;
    public $date_to_upload;

    public $date_from_create;
    public $date_to_create;

    /* public function __construct($isArchive)
    {
        parent::__construct();
        $this->isArchive = $isArchive;
    } */
    /**
     * {@inheritdoc}
     */

    public function rules()
    {
        return [
            [['id', 'country_id', 'created_at', 'updated_at', 'upload_at', 'author_id', 'show_it'], 'integer'],
            [['main_theme', 'sub_theme', 'body', Consts::STATUS_ATTR], 'safe'],
            [['date_from_upload', 'date_to_upload', 'date_from_create', 'date_to_create'], 'date', 'format' => 'php:d.m.Y'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Posts::find()
            ->select([
                'tour_posts.id',
                'country_id',
                'upload_at',
                'tour_posts.author_id',
                'main_theme',
                'sub_theme',
                'tour_posts.created_at',
                Consts::STATUS_ATTR,
                'show_it',
            ])->joinWith([
                'country',
                'author' => function ($query) {
                    $query->select(['id', 'username']);
                },
            ]);

        if ($this->isArchive) {
            $query = $query->where(['author_id' => Yii::$app->user->identity->id, 'show_it' => true, 'post_status' => Consts::FLOW_ARCHIVE]);
        }

        if (!$this->isArchive) {
            $query = $query->where(['author_id' => Yii::$app->user->identity->id, 'show_it' => true])->andWhere(['!=', 'post_status', Consts::FLOW_ARCHIVE]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country_id' => $this->country_id,
            'author_id' => $this->author_id,
            'show_it' => $this->show_it,
            Consts::STATUS_ATTR => $this->{Consts::STATUS_ATTR},
        ]);

        $query->andFilterWhere(['like', 'main_theme', $this->main_theme])
            ->andFilterWhere(['like', 'sub_theme', $this->sub_theme])
            ->andFilterWhere(['like', 'body', $this->body])
            ->andFilterWhere(['>=', 'upload_at', $this->date_from_upload ? strtotime($this->date_from_upload . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'upload_at', $this->date_to_upload ? strtotime($this->date_to_upload . ' 23:59:59') : null])
            ->andFilterWhere(['>=', 'created_at', $this->date_from_create ? strtotime($this->date_from_create . ' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $this->date_to_create ? strtotime($this->date_to_create . ' 23:59:59') : null]);

        return $dataProvider;
    }

    protected function frontQuery()          // запрос для пользовательской части (фронта), без отображения "просмотров", для не залогиненных
    {
        $query = Posts::find()
            ->select([
                'tour_posts.id',
                'country_id',
                'upload_at',
                'tour_posts.author_id',
                'main_theme',
                'sub_theme',
            ])->joinWith([
                'country',
                'author' => function ($query) {
                    $query->select(['id', 'username']);
                },
            ])->where(['show_it' => true])->andWhere(['<=', 'upload_at', strtotime("now")])
            ->orderBy(['upload_at' => SORT_DESC]);

        return $query;
    }
}
