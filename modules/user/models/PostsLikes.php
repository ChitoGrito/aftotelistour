<?php

namespace app\modules\user\models;

use Yii;
use app\modules\main\models\Posts;

/**
 * This is the model class for table "{{%posts_likes}}".
 *
 * @property int $id
 * @property int $post_id
 * @property int $author_id
 *
 * @property User $author
 * @property Posts $post
 */
class PostsLikes extends \yii\db\ActiveRecord
{
    protected $user_id;
    protected $current_posts_id;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%posts_likes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_id', 'author_id'], 'required'],
            [['post_id', 'author_id'], 'integer'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['post_id'], 'exist', 'skipOnError' => true, 'targetClass' => Posts::className(), 'targetAttribute' => ['post_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'post_id' => Yii::t('app', 'Post ID'),
            'author_id' => Yii::t('app', 'Author ID'),
        ];
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id']);
    }

    /**
     * Gets query for [[Post]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Posts::className(), ['id' => 'post_id']);
    }

    public function makeLike($user_id, $post_id)
    {
        $this->user_id = $user_id;
        $this->current_posts_id = $post_id;
        $likes = [];
        $isLiked = self::find()->where(['post_id' => $this->current_posts_id])->andWhere(['author_id' => $this->user_id])->one();
        if (empty($isLiked)) {
            $this->post_id = $this->current_posts_id;
            $this->author_id = $this->user_id;
            $this->save();
            $likes['isLiked'] = true;
        } else {
            $isLiked->delete();
            $likes['isLiked'] = false;
        }
        $likes['likes_count'] = self::find()->where(['post_id' => $this->current_posts_id])->count();
        return $likes;
    }
}
