<?php

return [
    'adminEmail' => 'robot@samaragips.adm',
    'supportEmail' => 'robot@samaragips.adm',
    'user.passwordResetTokenExpire' => 3600,
    'user.emailConfirmTokenExpire' => 259200,  // 3 days
    'footer.img.path' => 'web/img/footer/',
    'time.footer.message' => 60*10,             // один раз за это время можно посылать сообщение из подвала сайта
    'main.carusel.photos' => 'web/img/main-carusel/',
    'posts.photos' => '/img/posts/',
    'posts.files' => '/img/files/',
    'countries.icons' => 'web/img/countries/',
    'users.profile.photos' => 'web/img/users/',
];
