<?php
return [
    'Adress' => array(
        'ico' => 'icon-placepin',
        'rus' => 'Адрес',
    ),
    'Email' => array(
        'ico' => 'icon-mail',
        'rus' => 'E-mail',
    ),
    'Phone' => array(
        'ico' => 'icon-smartphone',
        'rus' => 'Телефон',
    ),
    'Facebook' => array(
        'ico' => 'icon-facebook',
        'rus' => 'Фейсбук',
    ),
];
