<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%navigationSubp2}}`.
 */
class m200601_171501_create_navigationSubp2_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%navigationSubp2}}', [
            'id' => $this->primaryKey(),
            'nav_nav_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->Null(),
            'access' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%navigationSubp2}}');
    }
}
