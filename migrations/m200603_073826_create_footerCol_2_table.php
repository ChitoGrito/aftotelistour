<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footerCol_2}}`.
 */
class m200603_073826_create_footerCol_2_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%footerCol_2}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->Null(),
            'ico' => $this->string()->Null(),
            'sub_title' => $this->string()->Null(),
            'content' => $this->string()->Null(),
            'show_it' => $this->boolean(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footerCol_2}}');
    }
}
