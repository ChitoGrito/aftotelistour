<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%main_carusel_images}}`.
 */
class m200611_071751_create_main_carusel_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%main_carusel_images}}', [
            'id' => $this->primaryKey(),
            'img_src' => $this->string(),
            'img_text_big' => $this->string(),
            'img_text_small' => $this->string(),
            'show_it' => $this->boolean()->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%main_carusel_images}}');
    }
}
