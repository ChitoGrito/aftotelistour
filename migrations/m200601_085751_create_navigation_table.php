<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%navigation}}`.
 */
class m200601_085751_create_navigation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%navigation}}', [
            'id' => $this->primaryKey(),
            'url' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'access' => $this->integer()->notNull(),
            'subparagraph' => $this->integer()->Null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%navigation}}');
    }
}
