<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footer_messages}}`.
 */
class m200609_054414_create_footer_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%footer_messages}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'name' => $this->string(30),
            'email' => $this->string(50)->notNull(),
            'text' => $this->text(),
            'user_ip' => $this->integer()->unsigned()->notNull(),
            'viewed' => $this->boolean()->null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footer_messages}}');
    }
}
