<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posts}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%countries}}`
 * - `{{%user}}`
 */
class m200613_150929_create_posts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%posts}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'upload_at' => $this->integer(),
            'author_id' => $this->integer(),
            'show_it' => $this->boolean(),
            'enable_likes' => $this->boolean(),
            'enable_dislikes' => $this->boolean(),
            'enable_coments' => $this->boolean(),
            'main_theme' => $this->string(255),
            'sub_theme' => $this->string(255),
            'body' => $this->text(),
            'post_status' => $this->string(40),
        ]);

        // creates index for column `country_id`
        $this->createIndex(
            '{{%idx-posts-country_id}}',
            '{{%posts}}',
            'country_id'
        );

        // add foreign key for table `{{%countries}}`
        $this->addForeignKey(
            '{{%fk-posts-country_id}}',
            '{{%posts}}',
            'country_id',
            '{{%countries}}',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            '{{%idx-posts-author_id}}',
            '{{%posts}}',
            'author_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-posts-author_id}}',
            '{{%posts}}',
            'author_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%countries}}`
        $this->dropForeignKey(
            '{{%fk-posts-country_id}}',
            '{{%posts}}'
        );

        // drops index for column `country_id`
        $this->dropIndex(
            '{{%idx-posts-country_id}}',
            '{{%posts}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-posts-author_id}}',
            '{{%posts}}'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            '{{%idx-posts-author_id}}',
            '{{%posts}}'
        );

        $this->dropTable('{{%posts}}');
    }
}
