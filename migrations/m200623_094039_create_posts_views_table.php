<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%posts_views}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%countries}}`
 * - `{{%user}}`
 */
class m200623_094039_create_posts_views_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        
        $this->createTable('{{%posts_views}}', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
        ], $tableOptions);

        // creates index for column `post_id`
        $this->createIndex(
            '{{%idx-posts_views-post_id}}',
            '{{%posts_views}}',
            'post_id'
        );

        // add foreign key for table `{{%posts}}`
        $this->addForeignKey(
            '{{%fk-posts_views-post_id}}',
            '{{%posts_views}}',
            'post_id',
            '{{%posts}}',
            'id',
            'CASCADE'
        );

        // creates index for column `author_id`
        $this->createIndex(
            '{{%idx-posts_views-author_id}}',
            '{{%posts_views}}',
            'author_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-posts_views-author_id}}',
            '{{%posts_views}}',
            'author_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%countries}}`
        $this->dropForeignKey(
            '{{%fk-posts_views-post_id}}',
            '{{%posts_views}}'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            '{{%idx-posts_views-post_id}}',
            '{{%posts_views}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-posts_views-author_id}}',
            '{{%posts_views}}'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            '{{%idx-posts_views-author_id}}',
            '{{%posts_views}}'
        );

        $this->dropTable('{{%posts_views}}');
    }
}
