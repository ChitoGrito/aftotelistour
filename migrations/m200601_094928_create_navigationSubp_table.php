<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%navigationSubp}}`.
 */
class m200601_094928_create_navigationSubp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%navigationSubp}}', [
            'id' => $this->primaryKey(),
            'nav_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'access' => $this->integer()->Null(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%navigationSubp}}');
    }
}
