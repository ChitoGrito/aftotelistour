<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%footerCol_1}}`.
 */
class m200603_073815_create_footerCol_1_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%footerCol_1}}', [
            'id' => $this->primaryKey(),
            'theme' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'img_link' => $this->string()->Null(),
            'img_src' => $this->string()->Null(),
            'read_more_link' => $this->string()->Null(),
            'show_it' => $this->boolean(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%footerCol_1}}');
    }
}
