<?php

namespace app\commands;

use app\components\rbac\CommentAuthorRule;
use app\components\rbac\PostAuthorRule;
use Yii;
use yii\console\Controller;
use app\modules\main\models\Posts;

/**
 * Инициализатор RBAC выполняется в консоли php yii my-rbac/init
 */
class MyRbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...

        // Создадим роли админа и редактора новостей
        $admin = $auth->createRole('admin');
        $editor = $auth->createRole('editor');
        $user = $auth->createRole('user');

        // запишем их в БД
        $auth->add($admin);
        $auth->add($editor);
        $auth->add($user);

        // Создаем наше правило, которое позволит проверить автора новости
        $authorRule = new CommentAuthorRule();
        
        // Запишем его в БД
        $auth->add($authorRule);

        $updateOwnComments = $auth->createPermission('updateOwnComments');
        $updateOwnComments->description = 'Редактирование собственного комента';

        // Указываем правило AuthorRule для разрешения updateOwnComments.
        $updateOwnComments->ruleName = $authorRule->name;

        $auth->add($updateOwnComments);




        $post = new Posts();
        $post->enterWorkflow();
        foreach ($post->getWorkflow()->getAllStatuses() as $flow) {                                     // перебор всех возможных переходов в цикле статьи, и назначение каждому разрешения, в з-ависимости от метаданных прописанных в обекте обьявления потока
            $flowStart = $flow->getId();                                                                // записываю начальный элемент (начало стрелки)
            $outGoingAcessRole = $flow->getMetadata('access');                                          // получаем имя текущего статуса
            $outGoingAccessLevell = $flow->getMetadata('accessLevel');                                  // получаем уровень доступа текущего статуса
            $transitions = $flow->getTransitions();                                                     // получаем все возможные стутсы, на которые можно перейти из текущего
            foreach ($transitions as $transition) {                                                     // перебираем все эти статусы (статусы "назначения")
                $endStatus = $transition->getEndStatus()->getId();                                      // получаем наименование статуса назначения
                $flowPermission = $auth->createPermission($flowStart.'->'.$endStatus);                // создаем разарешение перехода с "исходящего" статутуса на "следующий"
                $auth->add($flowPermission);                                                            // записываем его в БД
                $incommingAcessRole = $transition->getEndStatus()->getMetadata('access');               // получаем наименование роли доступа, для "следующего" статуса
                $incommingAcessLevell = $transition->getEndStatus()->getMetadata('accessLevel');        // получаем уровень доступа (int) для "следующего" статуса

                if ($incommingAcessLevell >= $outGoingAccessLevell) {                                   // сравниваем уровни доступа
                    $acessRole = $incommingAcessRole;                                                   // если уровень у "следующего" статуса больше, чем у "исходящего", назначаем разрешение для роли "следующего" статуса
                } else {
                    $acessRole = $outGoingAcessRole;                                                    // если нет, то назначаем наследование роли "исходящего" статуса
                }

                $auth->addChild(${$acessRole}, $flowPermission);
                $this->stdout('Creared permission: "'.$flowStart.' -> '.$endStatus.'" for role "'.$acessRole.'"' . PHP_EOL);
            }
        }

        // Теперь добавим наследования. Для роли editor мы добавим разрешение updateNews,
        // а для админа добавим наследование от роли editor и еще добавим собственное разрешение viewAdminPage

        // Роли «Редактор новостей» присваиваем разрешение «Редактирование новости»
        $auth->addChild($user, $updateOwnComments);
        $auth->addChild($editor, $user);

        // админ наследует роль редактора новостей. Он же админ, должен уметь всё! :D
        $auth->addChild($admin, $editor);

        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 1);
        $auth->assign($user, 7);
        $auth->assign($user, 8);
        $this->stdout('Done!' . PHP_EOL);
    }

    public function actionAddRule()
    {
        $auth = Yii::$app->authManager;
        $user = $auth->getRole('user');
        $postAuthorRule = new PostAuthorRule();
        $auth->add($postAuthorRule);
        $updateOwnPosts = $auth->createPermission('updateOwnPosts');
        $updateOwnPosts->description = 'Редактирование собственной статьи';
        $updateOwnPosts->ruleName = $postAuthorRule->name;
        $auth->add($updateOwnPosts);
        $auth->addChild($user, $updateOwnPosts);
        $this->stdout('Done!' . PHP_EOL);
    }
}
