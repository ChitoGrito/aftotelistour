<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\modules\main\components\widgets\NavigationWidget;
use app\modules\main\components\widgets\FooterWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <style type="text/css">
    html {
        font-size: unset;
    }
    </style>
    <?php $this->head() ?>
</head>
<body class="size-1140">
<?php $this->beginBody() ?>

    <header role="banner">    
        <nav class="background-white background-primary-hightlight">
            <div class="line">
                <div class="s-12 l-2">
                    <a href="<?= Url::home() ?>" class="logo"><img src="<?= Url::to(['@web/img/logo2.png']) ?>" alt=""></a>
                </div>
                <?= NavigationWidget::widget() ?>
            </div>
        </nav>
    </header>

    <?= $content ?>

    <?= FooterWidget::widget()?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
