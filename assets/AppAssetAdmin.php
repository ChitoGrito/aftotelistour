<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAssetAdmin extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = __DIR__;
    public $css = [
        'css/site_admin.css',
    ];
    public $js = [
        /* //'js/owl.carousel.min.js',
        'js/jquery-1.8.3.min.js',
        'js/jquery-ui.min.js',
        'js/responsee.js',
        'owl-carousel/owl.carousel.js',
        'js/template-scripts.js', */
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
