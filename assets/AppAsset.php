<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $sourcePath = __DIR__;
    public $css = [
        'css/components.css',
        'css/icons.css',
        'css/responsee.css',
        'owl-carousel/owl.carousel.css',
        'owl-carousel/owl.theme.css',
        'css/template-style.css',
        'css/site.css',
    ];
    public $js = [
        'js/responsee.js',
        'owl-carousel/owl.carousel.js',
        'js/template-scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}
